# Repositorio para el Trabajo Final de Grado
**Grado de Ingeniería Informática**

**Universidad Complutense de Madrid**

Curso 2021/22

## Autores
- Fernando Hermoso Cara
- Juan José Plaza Campillo
- Ignacio Pallás Gozálvez

## Descripción Breve
Hemos desarrollado una aplicación móvil para el seguimiento y control de población hipertensa. La aplicación es capaz de registrar, para cada usuario dado de alta, la medicación que este toma, generando recordatorios para que el paciente no olvide tomar su tratamiento. Además es posible registrar la tensión arterial indicando la periodicidad necesaria según cada paciente. Para generar adherencia al tratamiento, se ha confeccionado un programa incentivador mediante puntos en el que el usuario irá ascendiendo puestos en una clasificación general.

Por otro lado, se ha realizado una aplicación web para que los médicos puedan tener un control de sus pacientes y así poder llevar a cabo un mejor diagnóstico y seguimiento. En esta aplicación web se permite acceder a sus tomas de tensión más recientes, medicación registrada y contactar con el paciente en cuestión mediante un chat.


### Notas
Este repositorio se corresponde con la parte del *frontend* de la aplicación Android para los pacientes.
