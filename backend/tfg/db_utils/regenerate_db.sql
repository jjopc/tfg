-- Para ejecutar el script (tarda unos minutos, se debe ejecutar en al terminal, sino se puede bloquear el ordenador):
-- Desde la terminal navegamos hasta el directorio del script y ejecutamos:
--
-- sudo psql -U yatahta -h localhost -d yatahta -f regenerate_db.sql
--
-- Se supone que se ha creado un usuario 'yatahta' y una base de datos llamada 'yatahta'
-- pedirá la password del usuario yatahta si la tiene
--
-- Limpia la base de datos y reinicia las secuencias a 1
truncate yatahta_user cascade ;
truncate yatahta_drug cascade ;
alter sequence yatahta_user_id_seq restart with 1;
alter sequence yatahta_drug_id_seq restart with 1;
alter sequence yatahta_posology_id_seq restart with 1;
alter sequence yatahta_pressure_id_seq restart with 1;
alter sequence yatahta_message_id_seq restart with 1;
alter sequence yatahta_battles_id_seq restart with 1;
alter sequence yatahta_morisky_id_seq restart with 1;

-- ejecuta en orden las secuencias INSERT  para poblar la base de datos
-- estas instrucciones son propias de psql
\i data_users.sql
\i data_drugs.sql
\i data_posologies.sql
\i data_pressures.sql
\i data_messages.sql
\i data_morisky.sql
\i data_batles.sql