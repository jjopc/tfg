import requests
import json
import random
import datetime
import string
import re

user_drugs = set()
doctor_ids = list()


def generate_password(length):
    # define data
    lower = string.ascii_lowercase
    upper = string.ascii_uppercase
    num = string.digits

    # combine the data
    all = lower + upper + num

    # use random
    temp = random.sample(all, length)

    # create the password
    password = "".join(temp)

    # print the password
    return password


def generate_users():
    start_string = "INSERT INTO public.yatahta_user (id, nick, type, pwd, points, birthday, doctor_id) VALUES ("
    types = ['NM', 'DC']
    nicks = ['Anais', 'Alice', 'Betty', 'Bonny', 'Cary', 'Celia', 'Daisy', 'Danger', 'Dinna', 'Dudda', 'Elma', 'Ensy',
             'Enya', 'Fifi', 'Fanny', 'Gigi', 'Gilda', 'Grissy', 'Haila', 'Hanna', 'Heisha', 'Hilda', 'Honny', 'Ina',
             'Issa', 'Isais', 'Jessy', 'Jossy', 'Katty', 'Keisha', 'Kitty', 'Kora', 'Letty', 'Libby', 'Lilly', 'Lissy',
             'Loly', 'Lulu', 'Mariam', 'Melba', 'Melissa', 'Milly', 'Minny', 'Monna', 'Natty', 'Nelly', 'Nelsa',
             'Neissa', 'Nildha', 'Niry', 'Ossy', 'Penny', 'Puppy', 'Raisa', 'Renza', 'Rina', 'Rossy', 'Sady', 'Sarah',
             'Sarais', 'Sasha', 'Siddy', 'Sussy', 'Tattiana', 'Tenny', 'Vania', 'Vinnie', 'Vivi', 'Yaima', 'Yenny',
             'Yensy', 'Yessy', 'Yissel', 'Yina', 'Yoly', 'Andy', 'Anthony', 'Benny', 'Berty', 'Billy', 'Chato', 'Daddy',
             'Delphy', 'Dido', 'Elias', 'Fito', 'Franky', 'Galy', 'Gilbert', 'Henry', 'Indio', 'John', 'Johnny',
             'Kachito', 'Kristal', 'Lanny', 'Lenny', 'Lui', 'Manny', 'Michel', 'Misha', 'Mongo', 'Nando', 'Nino',
             'Noel', 'Osiel', 'Osmel', 'Oriol', 'Panchy', 'Pasiel', 'Peter', 'Pincho', 'Pupy', 'Ranier', 'Rassiel',
             'Renzo', 'Ricky', 'Robby', 'Roman', 'Romeo', 'Russo', 'Sam', 'Saniel', 'Sandro', 'Sonny', 'Tatty', 'Trend',
             'Tommy', 'Visha', 'Waldo', 'Wilfre', 'Yanni', 'Yanko', 'Yeiko', 'Yildo', 'Yoscar', 'Yunier']
    global doctor_ids
    with open("data_users.sql", 'w+', encoding="UTF-8") as file:
        for id, name in enumerate(nicks):
            user_type = random.choices(types, weights=(95, 5), k=1)[0]  # 95% probabilidad de ser NM
            # pwd = generate_password(random.randint(8, 16))
            id += 1
            points = random.randint(0, 100)
            birthday = datetime.date(random.randint(1945, 1985), random.randint(1, 12), random.randint(1, 28))
            pwd = '12345678'
            if id < 8:
                user_type = 'DC'
                file.write(f"{start_string}{id},\'{name}\',\'{user_type}\',\'{pwd}\',{points},\'{birthday}\', null);\n")
            else:  # Distribuyo los pacientes entre los doctores, pero me interesa saber los id's para los mensajes
                user_type = 'NM'
                if 8 <= id < 36:
                    file.write(
                        f"{start_string}{id},\'{name}\',\'{user_type}\',\'{pwd}\',{points},\'{birthday}\', 1);\n")
                elif 36 <= id < 54:
                    file.write(
                        f"{start_string}{id},\'{name}\',\'{user_type}\',\'{pwd}\',{points},\'{birthday}\', 2);\n")
                elif 54 <= id < 72:
                    file.write(
                        f"{start_string}{id},\'{name}\',\'{user_type}\',\'{pwd}\',{points},\'{birthday}\', 3);\n")
                elif 72 <= id < 90:
                    file.write(
                        f"{start_string}{id},\'{name}\',\'{user_type}\',\'{pwd}\',{points},\'{birthday}\', 4);\n")
                elif 90 <= id < 108:
                    file.write(
                        f"{start_string}{id},\'{name}\',\'{user_type}\',\'{pwd}\',{points},\'{birthday}\', 5);\n")
                elif 108 <= id < 126:
                    file.write(
                        f"{start_string}{id},\'{name}\',\'{user_type}\',\'{pwd}\',{points},\'{birthday}\', 6);\n")
                else:
                    file.write(
                        f"{start_string}{id},\'{name}\',\'{user_type}\',\'{pwd}\',{points},\'{birthday}\', 7);\n")


def generate_drugs():
    start_string = "INSERT INTO public.yatahta_drug (id, name, photo, dosage) VALUES (DEFAULT, "
    drugs_list = ['adiro', 'nolotil', 'efferalgan', 'gelocatil', 'augmentine', 'voltaren', 'lexatin', 'paracetamol',
                  'orfidal', 'dianben', 'neobrufen', 'trankimazin', 'ventolin', 'almax', 'flumil', 'sintrom',
                  'enantyum', 'cardyl', 'alapril', 'amlodipino', 'valsartan']
    drugs = set()
    with open("data_drugs.sql", 'w+', encoding="UTF-8") as file:
        for drug in drugs_list:
            response = requests.get(f"https://cima.aemps.es/cima/rest/medicamentos?nombre={drug}")
            todos = json.loads(response.text)
            for result in todos['resultados']:
                name = result['nombre']
                if name in drugs:  # Evita duplicados
                    continue
                else:
                    drugs.add(name)
                if 'fotos' in result.keys():
                    photo = result['fotos'][0]['url']
                else:
                    photo = ''
                if 'dosis' in result.keys():
                    dosage = result['dosis']
                else:
                    dosage = ''
                file.write(f"{start_string}\'{name}\',\'{photo}\',\'{dosage}\');\n")


def generate_users_drugs():
    """ Asigna medicamentos a usuario de forma aleatoria """
    global user_drugs  # Para evitar duplicados
    for i in range(250):
        user_id = random.randint(8,
                                 136)  # Había 137 nicks distintos en la lista en generate_users(), uso los de los usuarios NM
        drug_id = random.randint(1, 179)  # Obtiene 179 medicamentos de la API de la AEMPS en generate_drugs()
        user_drugs.add((user_id, drug_id))


def list_to_string(lista):
    result = str()
    if type(lista) == list:
        for s in lista:
            result += str(s) + ','
        result = result[0:-1]

    if result == '':
        return None

    return result


def generate_posologies():  # Podría haber creado funciones auxiliares...
    generate_users_drugs()
    start_string = "INSERT INTO public.yatahta_posology (id, start_time, start_date, end_date, x_times, " \
                   "frequency, days_of_week,days_of_month, months_of_year, drug_id, user_id) VALUES (DEFAULT, "

    frequency_choices = ['HR', 'DY', 'WK', 'FN', 'MT', 'YE', 'DM']
    week_days = ['MO', 'TU', 'WE', 'TH', 'FR', 'SA', 'SU']
    months = ['JA', 'FE', 'MA', 'AP', 'MY', 'JN', 'JL', 'AG', 'SE', 'OC', 'NV', 'DE']
    global user_drugs
    with open("data_posologies.sql", 'w+', encoding="UTF-8") as file:
        for user_id, drug_id in user_drugs:
            # Inicializo las variables que luego formarán la orden SQL
            start_time = datetime.time(random.randint(1, 12), 0, 0)  # por variabilidad
            """ Empieza a las 08:00:00 horas, el resto se calculan, es para los recordatorios """
            start_date = datetime.date(2022, 1, 1) + datetime.timedelta(days=random.randint(1, 240))  # por variabilidad
            end_date = datetime.date(2022, 12, 31) + datetime.timedelta(days=random.randint(1, 240))  # por variabilidad
            x_times = 0
            freq_choice = random.choices(frequency_choices, weights=(50, 14, 5, 5, 5, 1, 20), k=1)
            """
            50% para toma ca x horas HR, 14% para tomas diarias DY, 
            5% para tomas semanales, quincenales y mensuales WK, FN y MT, 
            1% para tomas anuales YE y 20% para tomas a demanda DM
            """
            days_of_week = list()
            days_of_month = list()
            months_of_year = list()

            if freq_choice[0] == 'HR':  # Cada X horas
                x_times = random.choice([4, 6, 8])  # Cada 4, 6 u 8 horas, la pauta más habitual
            elif freq_choice[0] == 'DY':  # Ciertos días a la semana
                x_times = random.choice(range(8))  # Número de días a la semana que toma el medicamento
                days_of_week = set()
                while len(days_of_week) < x_times:  # Selecciona los días de la semana que los toma
                    days_of_week.add(random.choice(week_days))
                days_of_week = list(days_of_week)
            elif freq_choice[0] == 'WK':  # Un día a la semana, es un subconjunto de la anterior
                x_times = 1
                days_of_week = [random.choice(week_days)]  # Selecciona un día a la semana
            elif freq_choice[0] == 'FN':  # Frecuencia quincenal
                x_times = 1
                days_of_month.append(random.choice(range(1, 29)))  # Selecciona un día entre 1 y 28 (por febrero)
            elif freq_choice[0] == 'MT':
                # Cada X meses, el resto se calcula
                x_times = 1
                months_of_year.append(months[start_date.month])  # Mes de inicio, el resto se calcula
                days_of_month.append(start_date.day)  # Selecciona un día entre 1 y 28 (por febrero)
            elif freq_choice[0] == 'YE':  # Anualmente, cierto mes y cierto día, a las 8:00:00 saltará el recordatorio
                x_times = 1
                months_of_year = [random.choice(months)]
                days_of_month.append(random.choice(range(1, 29)))  # Selecciona un día entre 1 y 28 (por febrero)

            """
            VALUES (DEFAULT, '08:00:00', '2022-01-15', '2022-05-31', 1, 'HR', 'MO,TU,WE', '1,5,21', 'JN,JL', 1, 1);
            """
            days_of_month = list_to_string(days_of_month)
            days_of_week = list_to_string(days_of_week)
            months_of_year = list_to_string(months_of_year)
            result = f"{start_string}\'{start_time}\',\'{start_date}\',\'{end_date}\',{x_times},\'{freq_choice[0]}\',"

            if days_of_week is None:
                result += "null, "
            else:
                result += f"\'{{{days_of_week}}}\',"

            if days_of_month is None:
                result += "null, "
            else:
                result += f"\'{{{days_of_month}}}\',"

            if months_of_year is None:
                result += "null, "
            else:
                result += f"\'{{{months_of_year}}}\',"

            result += f"{drug_id},{user_id});\n"
            file.write(result)


def generate_pressures():
    start_string = "INSERT INTO public.yatahta_pressure (id, systolic, diastolic, arm, bpm, date, user_id) VALUES (DEFAULT,"
    arms = ['LT', 'RT']
    test_date = datetime.date(2022, 1, 15)
    with open("data_pressures.sql", 'w', encoding="UTF-8") as file:
        for user_id in range(8, 138):  # Se crearon 137 usuarios en generate_user()
            for i in range(random.randint(1, 100)):
                systolic = random.randint(110, 145)
                diastolic = random.randint(65, 90)
                arm = random.choice(arms)
                bpm = random.randint(55, 85)
                date_taken = test_date + datetime.timedelta(days=i)
                file.write(f"{start_string}{systolic},{diastolic},\'{arm}\',{bpm},\'{date_taken}\',{user_id});\n")


def generate_messages():
    # Datos sacados de: https://rajpurkar.github.io/SQuAD-explorer/
    start_string = "INSERT INTO public.yatahta_message (id, date, text, receiver_id, transmitter_id) VALUES (DEFAULT,"
    date = datetime.datetime.now()

    with open("train-v2.0.json", "r") as file:
        data = json.load(file)
        with open("data_messages.sql", "w") as sql_file:
            for topic in range(0, 11):
                json_data = data['data'][topic]
                for qas in json_data['paragraphs']:
                    for question in qas['qas']:
                        patient_id = random.randint(8, 137)
                        if 8 <= patient_id < 36:
                            doctor_id = 1
                        elif 36 <= patient_id < 54:
                            doctor_id = 2
                        elif 55 <= patient_id < 72:
                            doctor_id = 3
                        elif 72 <= patient_id < 90:
                            doctor_id = 4
                        elif 90 <= patient_id < 108:
                            doctor_id = 5
                        elif 108 <= patient_id < 126:
                            doctor_id = 6
                        else:
                            doctor_id = 7

                        if len(question['answers']) > 0:
                            # Primero inserto la pregunta de paciente -> doctor y luego la respuesta al revés.
                            # question = question['question']
                            # answer = question['answers'][0]['text']
                            sql_file.write("%s'%s','%s',%d,%d);\n" % (start_string, str(datetime.datetime.now()),re.sub('\'', '\'\'',question['question']),doctor_id,patient_id))
                            sql_file.write("%s'%s','%s',%d,%d);\n" % (start_string, str(datetime.datetime.now()),re.sub('\'', '\'\'', question['answers'][0]['text']), patient_id,doctor_id))


def generate_moriskys():
    start_string = "INSERT INTO public.yatahta_morisky (answer1, answer2, answer3,answer4,date,\"isOk\",user_id) " \
                   "VALUES ("
    answers = ['true', 'false']

    with open("data_morisky.sql", "w") as sql_file:
        for user_id in range(8, 137):
            for i in range(random.randint(1, 4)):
                answer1 = random.choices(answers, weights=(20, 80), k=1)[0]
                answer2 = random.choices(answers, weights=(80, 20), k=1)[0]
                answer3 = random.choices(answers, weights=(20, 80), k=1)[0]
                answer4 = random.choices(answers, weights=(20, 80), k=1)[0]

                if (answer1 == 'false' and answer2 == 'true' and
                        answer3 == 'false' and answer4 == 'false'):
                    is_ok = 'true'
                else:
                    is_ok = 'false'

                test_date = datetime.datetime(2021, random.randint(1,12),random.randint(1,28),
                                              random.randint(0,23),random.randint(0,59), random.randint(0,59))
                
                sql_file.write(f"{start_string}{answer1},{answer2},{answer3},{answer4}"
                               f",\'{test_date}\',{is_ok},{user_id});\n")


def generate_battles():
    organ_choices = ['EM', 'HT', 'LG', 'ST', 'KD', 'PC', 'LV', 'VW', 'HR']
    start_string = "INSERT INTO public.yatahta_batles (answer1, answer2, answer3,answer4,date,\"isOk\",user_id) " \
                   "VALUES ("
    answers = ['true', 'false']

    with open("data_batles.sql", "w") as sql_file:
        for user_id in range(8, 137):
            for i in range(random.randint(1, 3)):
                answer1 = random.choices(answers, weights=(90, 10), k=1)[0]
                answer2 = random.choices(answers, weights=(90, 10), k=1)[0]
                answer3 = random.choices(organ_choices, weights=(1, 59, 5, 5, 20, 2.5, 2.5, 2.5, 2.5), k=1)[0]
                answer4 = random.choices(organ_choices, weights=(1, 20, 5, 5, 59, 2.5, 2.5, 2.5, 2.5), k=1)[0]

                if (answer1 == 'true' and answer2 == 'true' and
                        answer3 == 'HT' and answer4 == 'KD'):
                    is_ok = 'true'
                else:
                    is_ok = 'false'

                test_date = datetime.datetime(2021, random.randint(1,12),random.randint(1,28),
                                              random.randint(0,23),random.randint(0,59), random.randint(0,59))
                
                sql_file.write(f"{start_string}{answer1},{answer2},\'{answer3}\',\'{answer4}\'"
                               f",\'{test_date}\',{is_ok},{user_id});\n")
    


def print_menu():
    print("Opciones:")
    print("\t1 - Generar usuarios")
    print("\t2 - Generar medicamentos")
    print("\t3 - Generar posologías")
    print("\t4 - Generar tomas de presiones")
    print("\t5 - Generar mensajes")
    print("\t6 - Generar tests de Morisky-Green")
    print("\t7 - Generar tests de batalla")
    print("\n\n\t0 - SALIR")


def menu():
    while True:
        print_menu()
        option = int(input("Opción: "))
        if option not in range(8):
            print("Error, opción no válida!")
        elif option == 0:
            break
        elif option == 1:
            generate_users()
        elif option == 2:
            generate_drugs()
        elif option == 3:
            generate_posologies()
        elif option == 4:
            generate_pressures()
        elif option == 5:
            generate_messages()
        elif option == 6:
            generate_moriskys()
        elif option == 7:
            generate_battles()

        print('\n' * 100)


if __name__ == "__main__":
    menu()
