from django.contrib import admin

# Register your models here.
from .models import *
admin.site.register(Drug)
admin.site.register(Posology)
admin.site.register(User)
admin.site.register(Pressure)
admin.site.register(Message)
admin.site.register(Morisky)
admin.site.register(Batles)