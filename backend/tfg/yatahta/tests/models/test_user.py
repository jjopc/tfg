from django.test import TestCase

from yatahta.models import *

class UserModelTests(TestCase):

    @classmethod
    def setUpTestData(cls):
        p1 = Posology(dosage=600,units="MG",each_frequency=8,frequency="HR")
        p1.save()

        p2 = Posology(dosage="400",units="MG",frequency="DM")
        p2.save()

        p3 = Posology(dosage=20,units="MG",each_frequency=1,frequency="DY")
        p3.save()

        d1 = Drug(name="Ibuprofeno")
        d1.save()
        d1.posology_set.add(p1)
        d1.posology_set.add(p2)

        d2 = Drug(name="Enalapril")
        d2.save()
        d2.posology_set.add(p3)

        user1 = User(nick="user1",type="NM",pwd="12345678",points=3,birthday="1987-04-23")
        user1.save()
        user1.drugs.add(d1)
        user1.drugs.add(d2)

        user2 = User(nick="user2",type="NM",pwd="asdfasdf",points=23,birthday="1961-02-19")

    def test_insert_one_user_to_db(self):
        """ Testea que se insertan correctamente 2 usuarios a la base de datos """
        user1 = User.objects.first()
        self.assertEqual(user1, "Son iguales")
