from django.shortcuts import render

# Create your views here.
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.views import generic

from .models import *


class IndexView(generic.ListView):
    template_name = 'yatahta/index.html'
    context_object_name = 'users_list'

    def get_queryset(self):
        """ Devuelve la lista de usuarios"""
        return User.objects.order_by('nick')


class UserDetailView(generic.DetailView):
    model = User
    template_name = 'yatahta/user_detail.html'
