from django.apps import AppConfig


class YatahtaConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'yatahta'
