from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator
from django.utils.html import escape
from django.contrib.postgres.fields import ArrayField


# Create your models here.
class Drug(models.Model):
    """ Clase que define los medicamentos que toman los usuarios. """
    name = models.CharField(max_length=255, unique=True)
    photo = models.CharField(max_length=255, default='')
    dosage = models.CharField(max_length=60, default='')

    def clean(self):
        """ Escapa el texto por si contiene caracteres HTML """
        self.name = escape(self.name)

    def __str__(self):
        return self.name


class User(models.Model):
    """ Clase que define los usuarios de la aplicación. """

    drugs = models.ManyToManyField(Drug, through='Posology')
    """ Establece una relación many-to-many entre usuarios y medicamentos. Un usuario puede tomar
    muchos medicamentos, y un medicamento puede ser tomado por muchos usuarios.  La join-table
    es Posology """

    nick = models.CharField(max_length=12, unique=True)

    NORMAL = 'NM'
    DOCTOR = 'DC'
    USER_CHOICES = [
        (NORMAL, 'Normal'),
        (DOCTOR, 'Doctor'),
    ]
    type = models.CharField(
        max_length=2,
        choices=USER_CHOICES,
        default=NORMAL,
    )

    doctor = models.ForeignKey('self', null=True, related_name='patient', on_delete=models.SET_NULL)
    """ Esta relación O2M refleja el hecho de un doctor tiene varios pacientes asignados
    mientras que un paciente solo puede tener un doctor asignado """

    pwd = models.CharField(
        max_length=24,
        validators=[MinValueValidator(8)],
    )
    points = models.IntegerField(default=0)
    birthday = models.DateField()

    def clean(self):
        """ Escapa el texto por si contiene caracteres HTML """
        self.nick = escape(self.nick)

    def __str__(self):
        tipo = str()
        for key, value in self.USER_CHOICES:
            if key == self.type:
                tipo = value
        return f"{self.nick}, tipo: {tipo}"


class Posology(models.Model):
    """ Clase que define la posología de la toma de medicamentos. Esto es, la frecuencia de tomas y la dosis. """
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    drug = models.ForeignKey(Drug,on_delete=models.CASCADE)
    """ Establece una relación one-to-many entre un medicamento y su posología. """
    """
    La frecuencia de toma de medicamentos puede ser:
        - Diario -> x veces al día -> ESTABLECER UNA HORA Y SE CALCULAN LAS HORAS DE LAS TOMAS
        - Varios días a la semana 
            -> x veces a la semana -> ESCOGER LOS DÍAS Y LA HORA(S) DE LA TOMA
            -> Un día a la semana -> ESCOGER EL DÍA Y LA HORA(S) DE LA TOMA
        - Quincenalmente -> ESCOGER DÍA INICIO Y SE CALCULAN EL RESTO Y LA HORA(S) DE TOMA
        - Mensualmente -> ESCOGER EL DÍA DE INICIO Y SE CALCULAN EL RESTO Y LA HORA DE TOMAA
        - Anualmente -> ESCOGER EL DÍA DE INICIO Y SE CALCULA EL RESTO Y LA HORA DE TOMA
        - A demanda -> NO ES NECESARIO GUARDAR NADA MÁS
    """
    start_time = models.TimeField(null=True, auto_now_add=False, auto_now=False)
    start_date = models.DateField(null=True, auto_now=False, auto_now_add=False)
    end_date = models.DateField(null=True, auto_now=False, auto_now_add=False)
    x_times = models.IntegerField(default=0)
    HOURS = 'HR'
    DAYS = 'DY'
    WEEKS = 'WK'
    FORTNIGHTLY = 'FN'
    MONTHS = 'MT'
    YEARS = 'YE'
    ON_DEMAND = 'DM'
    FREQUENCY_CHOICES = [(HOURS, 'horas'),(DAYS, 'días'),(WEEKS, 'semanas'),(FORTNIGHTLY, 'quincenalmente'),(MONTHS, 'meses'),(ON_DEMAND, 'a demanda'),]
    frequency = models.CharField(
        max_length=2,
        choices=FREQUENCY_CHOICES,
        default=HOURS,
    )

    MONDAY = 'MO'
    TUESDAY = 'TU'
    WEDNESDAY = 'WE'
    THURSDAY = 'TH'
    FRIDAY = 'FR'
    SATURDAY = 'SA'
    SUNDAY = 'SU'
    WEEK_DAYS = [(MONDAY, 'lunes'), (TUESDAY, 'martes'), (WEDNESDAY, 'miércoles'), (THURSDAY, 'jueves'),
                 (FRIDAY, 'viernes'), (SATURDAY, 'sábado'), (SUNDAY, 'domingo')]
    days_of_week = ArrayField(
        ArrayField(models.CharField(max_length=2, choices=WEEK_DAYS, default=MONDAY),size=7),
        null=True
    )

    days_of_month = ArrayField(
        ArrayField(models.IntegerField()),
        null=True
    )

    JANUARY = 'JA'
    FEBRUARY = 'FE'
    MARCH = 'MA'
    APRIL = 'AP'
    MAY = 'MY'
    JUNE = 'JN'
    JULY = 'JL'
    AUGUST = 'AG'
    SEPTEMBER = 'SE'
    OCTOBER = 'OC'
    NOVEMBER = 'NV'
    DECEMBER = 'DE'
    MONTHS = [(JANUARY, 'enero'), (FEBRUARY, 'febrero'), (MARCH, 'marzo'), (APRIL, 'abril'), (MAY, 'mayo'),
    (JUNE, 'junio'), (JULY, 'julio'), (AUGUST, 'agosto'), (SEPTEMBER, 'septiembre'), (OCTOBER, 'octubre'),
              (NOVEMBER, 'noviembre'), (DECEMBER, 'diciembre')]
    months_of_year = ArrayField(
        ArrayField(models.CharField(max_length=2, choices=MONTHS, default=JANUARY),size=12),
        null=True
    )

    def __str__(self):
        frequency = str()
        for key, value in self.FREQUENCY_CHOICES:
            if key == self.frequency:
                frequency = value
        return f"{frequency}"


class Message(models.Model):
    """ Clase que define los mensajes que van a intercambiar los usuarios con los doctores. """
    transmitter = models.ForeignKey(
        User,
        related_name='trasnmitter',
        on_delete=models.CASCADE
    )
    """ Campo que identifica al emisor del mensaje. """
    receiver = models.ForeignKey(
        User,
        related_name='receiver',
        on_delete=models.CASCADE
    )
    """ Campo que identifica al receptor del mensaje. """
    date = models.DateTimeField(auto_now_add=True)
    text = models.TextField()

    def __str__(self):
        return f"{self.date}: {self.text}"


class Pressure(models.Model):
    """ Clase que define las tomas de tensión que van a realizar los usuarios de la aplicación. """
    LEFT = 'LT'
    RIGHT = 'RT'
    ARM_CHOICES = [
        ('LT', 'Izquierdo'),
        ('RT', 'Derecho')
    ]

    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE
    )
    """ Es el usuario al que pertenece la toma de tensión."""

    systolic = models.IntegerField(validators=[MinValueValidator(90), MaxValueValidator(170)])
    diastolic = models.IntegerField(validators=[MinValueValidator(50), MaxValueValidator(110)])
    arm = models.CharField(
        max_length=2,
        choices=ARM_CHOICES,
        default=LEFT,
    )
    bpm = models.IntegerField(validators=[MinValueValidator(35), MaxValueValidator(220)])
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        arm = str()
        for key, value in self.ARM_CHOICES:
            if key == self.arm:
                arm = value

        return f"Presión tomada en brazo {arm}.\nFecha: {self.date}\nSistólica: {self.systolic}\n" \
               f"Diastólica: {self.diastolic}\nPulsaciones: {self.bpm}"


class Morisky(models.Model):
    """ Clase que define el test de Morisky-Green. """
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE
    )
    """ Es el usuario al que pertenece el test realizado. """
    answer1 = models.BooleanField()
    answer2 = models.BooleanField()
    answer3 = models.BooleanField()
    answer4 = models.BooleanField()
    date = models.DateTimeField(auto_now_add=True)
    isOk = models.BooleanField()
    """ Sólo si las respuestas son False, True, False y False, este campo = True"""

    def __str__(self):
        return f"Fecha: {self.date}\nResultado de test de Morisky-Green: {self.isOk}"


class Batles(models.Model):
    """ Clase que define el test de Morisky-Green. """
    EMPTY = 'EM'
    HEART = 'HT'
    LUNG = 'LG'
    STOMACH = 'ST'
    KIDNEY = 'KD'
    PANCREAS = 'PC'
    LIVER = 'LV'
    VIEW = 'VW'
    HEARING = 'HR'
    ORGANS_CHOICES = [
        (EMPTY, ''),
        (HEART, 'Corazón'),
        (LUNG, 'Pulmones'),
        (STOMACH, 'Estómago'),
        (KIDNEY, 'Riñones'),
        (PANCREAS, 'Pancreas'),
        (LIVER, 'Hígado'),
        (VIEW, 'Vista'),
        (HEARING, 'Oído'),
    ]

    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE
    )
    """ Es el usuario al que pertenece el test realizado. """
    answer1 = models.BooleanField()
    answer2 = models.BooleanField()
    answer3 = models.CharField(
        max_length=2,
        choices=ORGANS_CHOICES,
        default=EMPTY,
    )
    answer4 = models.CharField(
        max_length=2,
        choices=ORGANS_CHOICES,
        default=EMPTY,
    )
    date = models.DateTimeField(auto_now_add=True)
    isOk = models.BooleanField()
    """ Sólo si las respuestas son True, True y selecciona dos órganos que se pueden ver afectados
     por la HTA este campo = True"""

    def __str__(self):
        return f"Fecha: {self.date}\nResultado de test de Batalla: {self.isOk}"
