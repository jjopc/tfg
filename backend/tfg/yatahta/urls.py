from django.urls import path

from . import views

app_name = 'yatahta'
urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('<int:pk>/', views.UserDetailView.as_view(), name='user_detail'),
]