package com.example.yata_hta.utilities;

import android.content.Context;
import android.view.Gravity;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.yata_hta.Pressures;
import com.example.yata_hta.temporary.Pressure;


import java.util.List;

public class TablePreassures {

    private TableLayout tableLayout;
    private Context context;
    private String [] header = {"Fecha", "Sistólica", "Diastolica", "Pulsaciones"};
    private List<Pressure> listPreassure;
    private TableRow tableRow;
    private TextView txtCell;
    private int indexColumn;
    private int indexRow;
    private boolean multicolor = false;
    int color1, color2;

    public TablePreassures(TableLayout tableLayout, Context context) {
        this.tableLayout = tableLayout;
        this.context = context;
    }



  /*  public void  addHeader (String [] header){
        this.header = header;
    }*/


    public void addData (List<Pressure> listPreassure){
       this.listPreassure = listPreassure;
       createDataTable();
    }

    private void newRow(){
        tableRow= new TableRow(context);
    }

    private void newCell(){
        txtCell= new TextView(context);
        txtCell.setGravity(Gravity.CENTER);
        txtCell.setTextSize(20.0f);
    }

    public void createHeader(){
        indexColumn = 0;
        newRow();
        while (indexColumn < header.length){
            newCell();
            txtCell.setText(header[indexColumn]);
            tableRow.addView(txtCell, newTableRowParams());
            indexColumn++;
        }
        tableLayout.addView(tableRow);
    }

    private void createDataTable(){
        String info;

        //recorro fila
        for(indexRow = 1; indexRow <= listPreassure.size(); indexRow++){
            newRow();
            //añado info columnas
            Pressure pse = listPreassure.get(indexRow-1);
                newCell();
                txtCell.setText(String.valueOf(pse.getFecha()));
                txtCell.setTextSize(15);
                tableRow.addView(txtCell,newTableRowParams());

                newCell();
                txtCell.setText( String.valueOf(pse.getSystolic()));
                txtCell.setTextSize(15);
                tableRow.addView(txtCell,newTableRowParams());

                newCell();
                txtCell.setText( String.valueOf(pse.getDiastolic()));
                txtCell.setTextSize(15);
                tableRow.addView(txtCell,newTableRowParams());

                newCell();
                txtCell.setText( String.valueOf(pse.getHeartbeats()));
                txtCell.setTextSize(15);
                tableRow.addView(txtCell,newTableRowParams());


            tableLayout.addView(tableRow);
        }

    }

    public void backGroundHeader(int color){
        indexColumn = 0;

        while (indexColumn < header.length){
           txtCell = getCell(0,indexColumn++);
           txtCell.setBackgroundColor(color);
        }
       // tableLayout.addView(tableRow);
    }


    public void backGroundTable (int color1, int color2){

        for(indexRow = 1; indexRow <= 0/*listPreassure.size()*/; indexRow++){
            multicolor = !multicolor;
            txtCell = getCell(indexRow,0);
            txtCell.setBackgroundColor((multicolor)?color1:color2);

            txtCell = getCell(indexRow,1);
            txtCell.setBackgroundColor((multicolor)?color1:color2);

            txtCell = getCell(indexRow,2);
            txtCell.setBackgroundColor((multicolor)?color1:color2);

            txtCell = getCell(indexRow,3);
            txtCell.setBackgroundColor((multicolor)?color1:color2);
        }
        this.color1 = color1;
        this.color2 = color2;
    }

    private TableRow getRow (int index){
        return (TableRow) tableLayout.getChildAt(index);
    }

    private TextView getCell (int rowIndex, int columnsIndex ){
        tableRow = getRow(rowIndex);
        return (TextView) tableRow.getChildAt(columnsIndex);
    }

    private TableRow.LayoutParams newTableRowParams(){
        TableRow.LayoutParams params = new TableRow.LayoutParams();
        params.setMargins(1,1,1,1);
        params.weight = 1;
        return params;
    }

}
