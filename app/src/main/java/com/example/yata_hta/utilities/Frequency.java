package com.example.yata_hta.utilities;

import androidx.annotation.NonNull;

import java.io.Serializable;

public enum Frequency implements Serializable {
    DAILY("Diariamente"),
    WEEKLY("Semanalmente"),
    MONTHLY("Mensualmente"),
    YEARLY("Anualmente");

    public String frequency;

    Frequency(String frequency) {
        this.frequency = frequency;
    }

    public String getFrequency() {
        return frequency;
    }

    @NonNull
    @Override
    public String toString() {
        return frequency;
    }
}
