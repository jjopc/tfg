package com.example.yata_hta.utilities;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.yata_hta.R;
import com.example.yata_hta.temporary.Pressure;
import com.example.yata_hta.temporary.UserPonit;

import java.util.List;

public class TableUserPoints {

    private TableLayout tableLayout;
    private Context context;
    private String [] header = {"Puesto","Usuario", "Puntuación"};
    private List<UserPonit> listPoint; // Lista de usuarios / puntos
    private TableRow tableRow;
    private TextView txtCell;
    private int indexColumn;
    private int indexRow;
    private String user;
    private boolean multicolor = false;
    int color1, color2;

    public TableUserPoints(TableLayout tableLayout, Context context, String user) {
        this.tableLayout = tableLayout;
        this.context = context;
        this.user = user;
    }

    public void addData (List<UserPonit> listPoint){
        this.listPoint = listPoint;
        createDataTable();
    }

    private void newRow(){
        tableRow= new TableRow(context);
    }

    private void newCell(){
        txtCell= new TextView(context);
        txtCell.setGravity(Gravity.CENTER);
        txtCell.setTextSize(20.0f);
    }


    public void createHeader(){
        indexColumn = 0;
        newRow();
        while (indexColumn < header.length){
            newCell();
            txtCell.setText(header[indexColumn]);
            tableRow.addView(txtCell, newTableRowParams());
            indexColumn++;
        }
        tableLayout.addView(tableRow);
    }

    private void createDataTable(){
        String info;

        //recorro fila
        for(indexRow = 1; indexRow <= listPoint.size(); indexRow++){

            newRow();
            //añado info columnas
            UserPonit pse = listPoint.get(indexRow-1);


            // si es el usuaria esta cambiar color
            if(pse.getUsername().equals(user)){

                newCell();
                txtCell.setText(String.valueOf(pse.getPosition()));
                txtCell.setTextSize(15);
                txtCell.setTextColor(Color.RED);
                tableRow.addView(txtCell,newTableRowParams());

                newCell();
                txtCell.setText(String.valueOf(pse.getUsername()));
                txtCell.setTextSize(15);
                txtCell.setTextColor(Color.RED);
                tableRow.addView(txtCell,newTableRowParams());

                newCell();
                txtCell.setText( String.valueOf(pse.getPoints()));
                txtCell.setTextSize(15);
                txtCell.setTextColor(Color.RED);
                tableRow.addView(txtCell,newTableRowParams());
                tableLayout.addView(tableRow);

            }else{

                newCell();
                txtCell.setText(String.valueOf(pse.getPosition()));
                txtCell.setTextSize(15);
                tableRow.addView(txtCell,newTableRowParams());

                newCell();
                txtCell.setText(String.valueOf(pse.getUsername()));
                txtCell.setTextSize(15);
                tableRow.addView(txtCell,newTableRowParams());

                newCell();
                txtCell.setText( String.valueOf(pse.getPoints()));
                txtCell.setTextSize(15);
                tableRow.addView(txtCell,newTableRowParams());
                tableLayout.addView(tableRow);

            }

        }
    }



    private TableRow getRow (int index){
        return (TableRow) tableLayout.getChildAt(index);
    }

    private TextView getCell (int rowIndex, int columnsIndex ){
        tableRow = getRow(rowIndex);
        return (TextView) tableRow.getChildAt(columnsIndex);
    }



    private TableRow.LayoutParams newTableRowParams(){
        TableRow.LayoutParams params = new TableRow.LayoutParams();
        params.setMargins(1,1,1,1);
        params.weight = 1;
        return params;
    }

    public void backGroundHeader(int color){
        indexColumn = 0;

        while (indexColumn < header.length){
            txtCell = getCell(0,indexColumn++);
            txtCell.setBackgroundColor(color);
        }
        // tableLayout.addView(tableRow);
    }
}
