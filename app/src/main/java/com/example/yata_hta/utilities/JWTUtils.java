package com.example.yata_hta.utilities;

import android.util.Base64;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;


public class JWTUtils {
    public static String decoded(String JWTEncoded)  {
        String ret = "";
        try {
            String[] split = JWTEncoded.split("\\.");
            Log.d("JWT_DECODED", "Header: " + getJson(split[0]));
            Log.d("JWT_DECODED", "Body: " + getJson(split[1]));

            ret = getJson(split[1]);
        }catch (UnsupportedEncodingException e){

        }


        return ret;
    }

    private static String getJson(String strEncoded) throws UnsupportedEncodingException{
        byte[] decodedBytes = Base64.decode(strEncoded, Base64.URL_SAFE);
        return new String(decodedBytes, "UTF-8");
    }



    // Me convierte el token decodificado y me extrae el user_id
    public static int stringToJSONUserID (String str){
        int payload = 0;
        try{
            JSONObject jsonObject = new JSONObject(str);
             payload = jsonObject.getInt("user_id");

        }catch (Exception e){
            e.printStackTrace();
        }
        return payload;
    }




    // Comprueba si el Token ha expirado
    public static Boolean isAccessTokenExpired(String token){

        try {
            JSONObject obj = new JSONObject(token);
            long expireDate = obj.getLong("exp");
            Timestamp timestampExpireDate= new Timestamp(expireDate*1000);

            long time = System.currentTimeMillis();
            Timestamp timestamp = new Timestamp(time);

            Boolean res = timestamp.after(timestampExpireDate);

            return  res;

        } catch (JSONException e) {
            e.printStackTrace();
            return true;
        }

    }




}
