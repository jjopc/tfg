package com.example.yata_hta.utilities;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.example.yata_hta.R;
import com.example.yata_hta.User;

public class AlarmReceiver extends BroadcastReceiver {

    private NotificationManager mNotificationManager;
    // Notification ID.
    private static final int NOTIFICATION_ID = 0;
    // Notification channel ID.
    private static final String PRIMARY_CHANNEL_ID = "primary_notification_channel";


    private static final String ACTION_UPDATE_NOTIFICATION = "primary_notification_channel";


    public AlarmReceiver() {    }

    @Override
    public void onReceive(Context context, Intent intent) {
        // Inicializamos el manager de las notificaiones
        mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        sendNotification(context);

    }

    /**
     * OnClick method for the "Notify Me!" button. Creates and delivers a simple notification.
     */
    public void sendNotification(Context context){
        Intent updateIntent = new Intent(ACTION_UPDATE_NOTIFICATION);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, NOTIFICATION_ID, updateIntent, PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder notifyBuilder = getNotificationBuilder(context);
        // Add the action button using the pending intent.
    //    notifyBuilder.addAction(R.drawable.ic_android, "SI", pendingIntent);

     //   mNotificationManager.notify(NOTIFICATION_ID, notifyBuilder.build());
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        notificationManager.notify(NOTIFICATION_ID, notifyBuilder.build());

    }


    private NotificationCompat.Builder getNotificationBuilder(Context context){

        Intent notificationIntent = new Intent(context, User.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent notificationPendingIntent = PendingIntent.getActivity(context, NOTIFICATION_ID, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);




        NotificationCompat.Builder notifyBuilder = new NotificationCompat.Builder(context, PRIMARY_CHANNEL_ID)
                .setContentTitle("YATA_HTA")
                .setContentText("Tómate la medicación !!")
                .setSmallIcon(R.drawable.ic_android)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setLights(Color.MAGENTA, 1000,1000)
                .setVibrate(new long[]{1000,1000,1000,1000})
                .setAutoCancel(true)
                .setContentIntent(notificationPendingIntent)
                .setDefaults(Notification.DEFAULT_SOUND);
        return notifyBuilder;
    }



}

