package com.example.yata_hta.utilities;

import androidx.annotation.NonNull;

import java.io.Serializable;

public enum DosageUnits implements Serializable {
    MILLILITERS("mililitros"),
    LITERS("litros"),
    MILLIGRAMS("miligramos"),
    GRAMS("gramos");

    private String units;

    DosageUnits(String units) {
        this.units = units;
    }

    public String getUnits() {
        return units;
    }


    @NonNull
    @Override
    public String toString() {
        return units;
    }
}
