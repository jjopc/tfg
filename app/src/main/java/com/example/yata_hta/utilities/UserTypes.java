package com.example.yata_hta.utilities;

/**
 * Este enumerado refleja los tipos de usuarios que tenemos en nuestra aplicaión:
 *
 * @author Juan José Plaza Campillo
 * @author Fernando Hermoso Cara
 * @author Ignacio Pallás Gozalvez
 *
 * <p></p>
 * <p>NORMAL: es un usuario con rol "normal" que va a registrar su tensión y medicación con nuestra aplicación.</p>
 * <p>DOCTOR: es un usuario con rol "médico", estos pueden consultar datos y registros de usuarios normales.</p>
 */
public enum UserTypes {
    NORMAL,
    DOCTOR
}
