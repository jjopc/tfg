package com.example.yata_hta.utilities;

import androidx.annotation.NonNull;

import java.io.Serializable;

public enum WeekDays implements Serializable {
    SUNDAY("domingo"),
    MONDAY("lunes"),
    TUESDAY("martes"),
    WEDNESDAY("miércoles"),
    THURSDAY("jueves"),
    FRIDAY("viernes"),
    SATURDAY("sábado");

    private final String name;

    WeekDays(String name) {
        this.name = name;
    }

    public String getName() {return name;}


    @NonNull
    @Override
    public String toString() {
        return name;
    }
}
