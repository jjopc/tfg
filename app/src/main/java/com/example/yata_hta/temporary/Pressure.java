package com.example.yata_hta.temporary;

import com.example.yata_hta.Chat;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class Pressure implements Serializable {
    private float systolic;
    private float diastolic ;
    private int heartbeats;
    private String arm;
    private String comment;
    private String fecha;


    public Pressure(){
        this.comment = null;
    }
    public Pressure (float systolic, float diastolic,int heartbeats, String arm,String comment,String fecha ){
        this.systolic = systolic;
        this.diastolic = diastolic;
        this.heartbeats= heartbeats;
        this.arm = arm;
        this.comment = comment;
        this.fecha = fecha;
    }

    public String getFecha() {
        int i =0;
        String good= "";
        Character a = fecha.charAt(0);
        boolean encontrado = false;
        while (i<fecha.length() && a!= 'T'){
            good += a;
            i++;

            a = fecha.charAt(i);
        }

        return good;
    }


    public void setFecha(String fecha) { this.fecha = fecha; }

    public float getSystolic(){ return this.systolic; }
    public void setSystolic(float sys){this.systolic= sys;}

    public float getDiastolic() { return diastolic;}
    public void setDiastolic(float diastolic) { this.diastolic = diastolic; }

    public int getHeartbeats() { return heartbeats;}
    public void setHeartbeats(int heartbeats) { this.heartbeats = heartbeats;}

    public String getArm() { return arm;}
    public void setArm(String arm) { this.arm = arm;}

    public String getComment() { return comment;}
    public void setComment(String comment) { this.comment = comment;}
}
