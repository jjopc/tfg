package com.example.yata_hta.temporary;

public class ChatMessage {
    public String senderId, receivedId, message, dataTime;

    public ChatMessage(String senderId,String receivedId, String message, String dataTime) {
        this.senderId = senderId;
        this.message = message;
        this.dataTime = dataTime;
        this.receivedId = receivedId;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getReceivedId() {
        return receivedId;
    }

    public void setReceivedId(String receivedId) {
        this.receivedId = receivedId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDataTime() {
        return dataTime;
    }

    public void setDataTime(String dataTime) {
        this.dataTime = dataTime;
    }
}
