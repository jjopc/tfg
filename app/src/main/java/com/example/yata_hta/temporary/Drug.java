package com.example.yata_hta.temporary;

import android.util.Log;

import androidx.room.PrimaryKey;

import com.example.yata_hta.AddNewDrugActivity;
import com.example.yata_hta.utilities.DosageUnits;
import com.example.yata_hta.utilities.Frequency;
import com.example.yata_hta.utilities.WeekDays;

import java.io.Serializable;
import java.security.PrivateKey;
import java.util.Arrays;
import java.util.List;

import ca.antonious.materialdaypicker.MaterialDayPicker;

public class Drug implements Serializable {
    private static final String TAG = Drug.class.getSimpleName();

    private String id;
    private String drugName;
    private boolean at_breakfast_taken;
    private String at_breakfast;
    private boolean at_lunch_taken;
    private String at_lunch;
    private boolean at_dinner_taken;
    private String at_dinner;
    private boolean at_bedtime_taken;
    private String at_bedtime;
    private String start_date;
    private String end_date;
    private String userID;
    private String drugID;


    public Drug(String id, String drugName, boolean at_breakfast_taken, String at_breakfast,
                boolean at_lunch_taken, String at_lunch, boolean at_dinner_taken, String at_dinner,
                boolean at_bedtime_taken, String at_bedtime, String start_date, String end_date,
                String userID, String drugID) {
        this.id = id;
        this.drugName = drugName;
        this.at_breakfast_taken = at_breakfast_taken;
        this.at_breakfast = at_breakfast;
        this.at_lunch_taken = at_lunch_taken;
        this.at_lunch = at_lunch;
        this.at_dinner_taken = at_dinner_taken;
        this.at_dinner = at_dinner;
        this.at_bedtime_taken = at_bedtime_taken;
        this.at_bedtime = at_bedtime;
        this.start_date = start_date;
        this.end_date = end_date;
        this.userID = userID;
        this.drugID = drugID;
    }

    public boolean isAt_breakfast_taken() {
        return at_breakfast_taken;
    }

    public void setAt_breakfast_taken(boolean at_breakfast_taken) {
        this.at_breakfast_taken = at_breakfast_taken;
    }

    public boolean isAt_lunch_taken() {
        return at_lunch_taken;
    }

    public void setAt_lunch_taken(boolean at_lunch_taken) {
        this.at_lunch_taken = at_lunch_taken;
    }

    public boolean isAt_dinner_taken() {
        return at_dinner_taken;
    }

    public void setAt_dinner_taken(boolean at_dinner_taken) {
        this.at_dinner_taken = at_dinner_taken;
    }

    public boolean isAt_bedtime_taken() {
        return at_bedtime_taken;
    }

    public void setAt_bedtime_taken(boolean at_bedtime_taken) {
        this.at_bedtime_taken = at_bedtime_taken;
    }

    public String getDrugName() {
        return drugName;
    }

    public void setDrugName(String drugName) {
        this.drugName = drugName;
    }

    public static String getTAG() {
        return TAG;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAt_breakfast() {
        return at_breakfast;
    }

    public void setAt_breakfast(String at_breakfast) {
        this.at_breakfast = at_breakfast;
    }

    public String getAt_lunch() {
        return at_lunch;
    }

    public void setAt_lunch(String at_lunch) {
        this.at_lunch = at_lunch;
    }

    public String getAt_dinner() {
        return at_dinner;
    }

    public void setAt_dinner(String at_dinner) {
        this.at_dinner = at_dinner;
    }

    public String getAt_bedtime() {
        return at_bedtime;
    }

    public void setAt_bedtime(String at_bedtime) {
        this.at_bedtime = at_bedtime;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }



    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getDrugID() {
        return drugID;
    }

    public void setDrugID(String drugID) {
        this.drugID = drugID;
    }
}
