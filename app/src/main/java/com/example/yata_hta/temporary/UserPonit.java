package com.example.yata_hta.temporary;

public class UserPonit {
    private String position;
    private String username;
    private String points;

    public UserPonit(String position, String username, String points) {
        this.position = position;
        this.username = username;
        this.points = points;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }
}
