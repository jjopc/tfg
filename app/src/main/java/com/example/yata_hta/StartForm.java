package com.example.yata_hta;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class StartForm extends AppCompatActivity {

    private RadioButton moreTen;
    private RadioButton lessTen;
    private Button next;
    private Context context;
    private RadioGroup radioGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_form);
        init();
    }

    private void init() {
        moreTen = findViewById(R.id.masdiez);
        lessTen = findViewById(R.id.menosdiez);
        next = findViewById(R.id.next);
        radioGroup = findViewById(R.id.rg1);
        context = this;

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                if (moreTen.isChecked()){
                    startActivity(new Intent(context, MoriskyForm_1.class));
                }else if(lessTen.isChecked()){
                    startActivity(new Intent(context, BattleForm_1.class));
                }else{
                    Toast.makeText(context, " Seleccione una opcion", Toast.LENGTH_LONG).show();
                }
            }
        });

    }
}