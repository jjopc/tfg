package com.example.yata_hta;

import static java.lang.Integer.parseInt;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.yata_hta.temporary.Drug;
import com.example.yata_hta.temporary.UserPonit;
import com.example.yata_hta.utilities.TableUserPoints;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class User extends AppCompatActivity {
    public static final int ADD_DRUG_REQUEST = 1;
    private static final String TAG = User.class.getSimpleName();
    private String access = "";
    private String refresh = "";
    AppCompatImageView logout;
    AppCompatImageView alarms;
    Context context;


    private SharedPreferences preferences;
    SharedPreferences.Editor editor;

    private String userID;
    private String URL = "https://yatahta.juanjodev.es/api/users/";
    String URL_USER = "https://yatahta.juanjodev.es/api/users/users_count/";
    private String token = "";

    BottomNavigationView navigation;

    private TableLayout tableLayout;
    private List<Drug> drugs = new ArrayList<>();
    private List<UserPonit> clasification = new ArrayList<>();


    private String nameUser;
    private TextView name;
    private TextView pointsUser;
    private TextView myPosition;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_view);
        init();

    }
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(context, User.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getUserClasification();
        getUserCount();
    }

    private void init(){

        preferences= this.getSharedPreferences("LOGIN",this.MODE_PRIVATE);
        editor = preferences.edit();

        name = findViewById(R.id.name);
        pointsUser = findViewById(R.id.points);
        myPosition = findViewById(R.id.myPosition);
        logout = findViewById(R.id.logout);
        alarms = findViewById(R.id.alarm);
        context = this;

        access = preferences.getString("access","");
        refresh = preferences.getString("refresh","");
        token = preferences.getString("access" ,"");

        userID = String.valueOf(preferences.getInt("userID",0));
        nameUser = preferences.getString("userName","");
        name.setText(nameUser);
        URL +=userID+ "/user_classification/";


        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                AlertDialog.Builder alert = new AlertDialog.Builder(context);
                alert.setMessage("¿Desea cerrar sesión?")
                        .setCancelable(false)
                        .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                editor.putBoolean("checked",false);
                                editor.apply();
                                Intent intent = new Intent(context, Login.class);
                                startActivity(intent);
                            }
                        })
                        .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });

                AlertDialog titulo = alert.create();
                titulo.setTitle("Cerrar sesion");
                titulo.show();
            }
        });

        alarms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AlarmSettings.class);
                startActivity(intent);
            }
        });
        navigation = findViewById(R.id.bottom_navigation);
        navigation.setSelectedItemId(R.id.user);
        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.pressure:
                        startActivity(new Intent(getApplicationContext(), Pressures.class));
                        overridePendingTransition(0, 0);
                        return true;
                    case R.id.medication:
                        Intent intent = new Intent(User.this, DrugsMainView.class);
                        intent.putExtra("drugsList", (Serializable) drugs);
                        startActivityForResult(intent, ADD_DRUG_REQUEST);
                        overridePendingTransition(0, 0);
                        return true;
                    case R.id.user:
                        return true;
                    case R.id.mail:
                        startActivity(new Intent(getApplicationContext(), Chat.class));
                        overridePendingTransition(0, 0);
                        return true;

                }
                return false;
            }
        });
    }


    private void getUserClasification() {

        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, URL, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        for (int i = 0 ; i < response.length(); i++){
                            try {
                                JSONObject mensaje = response.getJSONObject(i);
                                String position;
                                String user;
                                String points;


                                if (!mensaje.getString("username").equals("...")){

                                    if (mensaje.getString("username").equals(nameUser)){
                                        String pos = mensaje.getString("position");
                                        myPosition.setText(pos);
                                        pointsUser.setText(mensaje.getString("points"));
                                    }

                                     position = mensaje.getString("position");
                                     user = mensaje.getString("username");
                                     points = mensaje.getString("points");

                                    UserPonit userPonit = new UserPonit(position,user,points);

                                    clasification.add(userPonit);
                                } else{
                                    UserPonit userPonit = new UserPonit("-","-","-");
                                    clasification.add(userPonit);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        crateTableLayout();

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof ServerError) {
                            Log.d("TAG", "Error en servidor");
                        }
                        if (error instanceof NoConnectionError) {//no conexion internet
                            Log.d("TAG", "No hay conexion a internet");
                        }

                        if (error instanceof NetworkError) {//desconexionsocket
                            Log.d("TAG", "NetWorkError");
                        }

                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer" + " " + token);//App.getToken()

                return headers;
            }
        };

        VolleySingleton.getInstaceVolley(getApplicationContext()).addToRequestQueue(request);


    }


    private void  getUserCount(){

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, URL_USER, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            String pos= myPosition.getText().toString();
                            String  userCount =  String.valueOf(response.getInt("users_count"));
                            pos += " de " + userCount;
                            myPosition.setText(pos);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(error instanceof ServerError){
                            Log.d("TAG","Error en servidor");
                        }
                        if(error instanceof NoConnectionError){//no conexion internet
                            Log.d("TAG","No hay conexion a internet");
                        }

                        if(error instanceof NetworkError){//desconexionsocket
                            Log.d("TAG","NetWorkError");
                        }

                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer" + " " + token);//App.getToken()

                return headers;
            }
        };

        VolleySingleton.getInstaceVolley(getApplicationContext()).addToRequestQueue(request);
    }

    private void crateTableLayout() {
        tableLayout = (TableLayout) findViewById(R.id.table);

        TableUserPoints tablePoint = new TableUserPoints(tableLayout, getApplicationContext(),nameUser);
        tablePoint.createHeader();
        tablePoint.addData(clasification);
        tablePoint.backGroundHeader(R.color.preassureTable_header);

    }


}