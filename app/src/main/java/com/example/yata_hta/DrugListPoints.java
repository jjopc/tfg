package com.example.yata_hta;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.yata_hta.recyclerViewAdapters.DrugPointsAdapter;
import com.example.yata_hta.recyclerViewAdapters.DrugRecyclerViewAdapter;
import com.example.yata_hta.recyclerViewAdapters.SearchDrugsAdapter;
import com.example.yata_hta.temporary.Drug;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DrugListPoints extends AppCompatActivity {
    private Context context;

    private String token = "";
    private String userID;
    private SharedPreferences preferences;
    private SharedPreferences preferencesPoints;
    private SharedPreferences.Editor editor;
    private BottomNavigationView navigation;
    private BottomNavigationView drugNavigation;

    public List<Drug> drugs;
    private List<Drug> drugsBreackfast;
    private List<Drug> drugLunch;
    private List<Drug> drugDinner;
    private List<Drug> drugBed;

    private RecyclerView RecyclerViewBreakfast;
    private RecyclerView RecyclerViewLunch;
    private RecyclerView RecyclerViewDinner;
    private RecyclerView RecyclerViewBed;

    private DrugPointsAdapter adapterBreakfast;
    private DrugPointsAdapter adapterLunch;
    private DrugPointsAdapter adapterDinner;
    private DrugPointsAdapter adapterBed;


    private Button addPoints;

//    public static int contBreackfast;
//    public static int contLunch;
//    public static int contDinner;
//    public static int contBed;
//
//
//    private static int contPoints;


    String URL = "https://yatahta.juanjodev.es/api/users/";
//    String URL_DRUGS = "https://yatahta.juanjodev.es/api/users/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drug_list_points);
        init();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(context, DrugsMainView.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        for (Drug d : drugs) {

            if (!d.getAt_breakfast().equals("null") && !d.getAt_breakfast().equals("0")) {
                drugsBreackfast.add(d);

            }

            if (!d.getAt_lunch().equals("null") && !d.getAt_lunch().equals("0")) {
                drugLunch.add(d);
            }

            if (!d.getAt_dinner().equals("null") && !d.getAt_dinner().equals("0")) {
                drugDinner.add(d);
            }

            if (!d.getAt_bedtime().equals("null") && !d.getAt_bedtime().equals("0")) {
                drugBed.add(d);
            }

        }


    }

//    @Override
//    protected void onResume () {
//        super.onResume();
//        getDrugs();
//
//
//
//
//    }


    private void init() {
        context = this;
        //SharePrefereces
        preferences = this.getSharedPreferences("LOGIN", this.MODE_PRIVATE);
        preferencesPoints = this.getSharedPreferences("POINTS", this.MODE_PRIVATE);
        editor = preferences.edit();
        token = preferences.getString("access", "");
        userID = String.valueOf(preferences.getInt("userID", 0));
        URL += userID + "/add_points/";
//        URL_DRUGS += userID + "/posologies/";


        drugs = new ArrayList<Drug>();
        drugs = (ArrayList<Drug>) getIntent().getSerializableExtra("listaMedicacion");
        drugsBreackfast = new ArrayList<Drug>();
        drugLunch = new ArrayList<Drug>();
        drugDinner = new ArrayList<Drug>();
        drugBed = new ArrayList<Drug>();


        adapterBreakfast = new DrugPointsAdapter(context,"breakfast");
        adapterLunch = new DrugPointsAdapter(context,"lunch");
        adapterDinner = new DrugPointsAdapter(context,"dinner");
        adapterBed = new DrugPointsAdapter(context,"bed");

        RecyclerViewBreakfast = findViewById(R.id.drugs_recycler_view_breakfast);
        RecyclerViewBreakfast.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        RecyclerViewBreakfast.setAdapter(adapterBreakfast);
        adapterBreakfast.setListDrus(drugsBreackfast);

        RecyclerViewLunch = findViewById(R.id.drugs_recycler_view_Lunch);
        RecyclerViewLunch.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        RecyclerViewLunch.setAdapter(adapterLunch);
        adapterLunch.setListDrus(drugLunch);

        RecyclerViewDinner = findViewById(R.id.drugs_recycler_view_Dinner);
        RecyclerViewDinner.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        RecyclerViewDinner.setAdapter(adapterDinner);
        adapterDinner.setListDrus(drugDinner);

        RecyclerViewBed = findViewById(R.id.drugs_recycler_view_bed);
        RecyclerViewBed.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        RecyclerViewBed.setAdapter(adapterBed);
        adapterBed.setListDrus(drugBed);


        addPoints = findViewById(R.id.sumarPuntos);
        addPoints.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                AlertDialog.Builder alert = new AlertDialog.Builder(context);
                alert.setMessage("¿Desea obtener los puntos de hoy?")
                        .setCancelable(false)
                        .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                boolean[] breakfastChechedItemList = adapterBreakfast.getChechedItemList();
                                boolean[] lunchChechedItemList = adapterLunch.getChechedItemList();
                                boolean[] dinnerChechedItemList = adapterDinner.getChechedItemList();
                                boolean[] bedChechedItemList = adapterBed.getChechedItemList();


                                for (int i = 0; i < breakfastChechedItemList.length; i++) {
                                    if (breakfastChechedItemList[i]) {
                                        Drug drug = drugsBreackfast.get(i);
                                        int drugId = Integer.parseInt(drug.getDrugID());
                                        int id = Integer.parseInt(drug.getId());
                                        patchValueTakenOfPosologies(drugId, id, "at_breakfast_taken");
                                    }
                                }
                                //
                                for (int i = 0; i < lunchChechedItemList.length; i++) {
                                    if (lunchChechedItemList[i]) {
                                        Drug drug = drugLunch.get(i);
                                        int drugId = Integer.parseInt(drug.getDrugID());
                                        int id = Integer.parseInt(drug.getId());
                                        patchValueTakenOfPosologies(drugId, id, "at_lunch_taken");
                                    }
                                }

                                //

                                for (int i = 0; i < dinnerChechedItemList.length; i++) {
                                    if (dinnerChechedItemList[i]) {
                                        Drug drug = drugDinner.get(i);
                                        int drugId = Integer.parseInt(drug.getDrugID());
                                        int id = Integer.parseInt(drug.getId());
                                        patchValueTakenOfPosologies(drugId, id, "at_dinner_taken");
                                    }
                                }

                                //
                                for (int i = 0; i < bedChechedItemList.length; i++) {
                                    if (bedChechedItemList[i]) {
                                        Drug drug = drugBed.get(i);
                                        int drugId = Integer.parseInt(drug.getDrugID());
                                        int id = Integer.parseInt(drug.getId());
                                        patchValueTakenOfPosologies(drugId, id, "at_bedtime_taken");
                                    }
                                }

                                final Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        postPoints();
                                        Intent intent = new Intent(context, User.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                },1000);

                            }
                        })
                        .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });

                AlertDialog titulo = alert.create();
                titulo.setTitle("Obtener puntos");
                titulo.show();
            }
        });

        navigation = findViewById(R.id.bottom_navigation);
        navigation.setSelectedItemId(R.id.medication);
        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.pressure:
                        startActivity(new Intent(getApplicationContext(), Pressures.class));
                        overridePendingTransition(0, 0);
                        return true;
                    case R.id.medication:
                        return true;
                    case R.id.user:
                        startActivity(new Intent(getApplicationContext(), User.class));
                        overridePendingTransition(0, 0);
                        return true;
                    case R.id.mail:
                        startActivity(new Intent(getApplicationContext(), Chat.class));
                        overridePendingTransition(0, 0);
                        return true;

                }
                return false;
            }
        });

        drugNavigation = findViewById(R.id.medication_mode);
        drugNavigation.setSelectedItemId(R.id.medication_points);
        drugNavigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.medication_points:
                        return true;
                    case R.id.medication2:
                        startActivity(new Intent(getApplicationContext(), DrugsMainView.class));
                        overridePendingTransition(0, 0);
                        return true;
                }
                return false;
            }
        });
    }


    private void patchValueTakenOfPosologies(int idDrug, int idPosologie, String clave) {
        JSONObject param = new JSONObject();
        String URL_PATCH = "https://yatahta.juanjodev.es/api/posologies/";
        URL_PATCH += String.valueOf(idPosologie) +"/";

        try {
//            param.put("id",idPosologie);
            param.put(clave, true);
            param.put("user", userID);
            param.put("drug", idDrug);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.PATCH, URL_PATCH, param,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        parseVolleyError(error);
//                    if(error instanceof ServerError){
//                        Log.d("TAG",error.toString());
//                    }
//                    if(error instanceof NoConnectionError){//no conexion internet
//                        Log.d("TAG","No hay conexion a internet");
//                    }
//                    if(error instanceof NetworkError){//desconexionsocket
//                        Log.d("TAG","NetWorkError");
//                    }
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer" + " " + token);//App.getToken()
                return headers;
            }
        };
        VolleySingleton.getInstaceVolley(getApplicationContext()).addToRequestQueue(request);
    }


    private void postPoints() {


        JSONObject param = new JSONObject();
        try {
            int totalDrugs = drugsBreackfast.size() + drugLunch.size() + drugDinner.size() + drugBed.size();
            int totalPoints = ((adapterBreakfast.getPoints() + adapterLunch.getPoints() + adapterDinner.getPoints() + adapterBed.getPoints()) * 100) / totalDrugs;
            param.put("points", totalPoints);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.PATCH, URL, param,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Intent intent = new Intent(context, User.class);
                        startActivity(intent);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        parseVolleyError(error);
//                        if (error instanceof ServerError) {
//                            Log.d("TAG", error.toString());
//                        }
//                        if (error instanceof NoConnectionError) {//no conexion internet
//                            Log.d("TAG", "No hay conexion a internet");
//                        }
//                        if (error instanceof NetworkError) {//desconexionsocket
//                            Log.d("TAG", "NetWorkError");
//                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer" + " " + token);//App.getToken()
                return headers;
            }
        };

        VolleySingleton.getInstaceVolley(getApplicationContext()).addToRequestQueue(request);
    }

    private void parseVolleyError(VolleyError error) {
        try {
            String responseBody = new String(error.networkResponse.data, "utf-8");
            JSONObject data = new JSONObject(responseBody);


        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}