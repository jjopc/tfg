package com.example.yata_hta;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class VolleySingleton {

    private static VolleySingleton instanceVolley;
    private RequestQueue request;
    private static Context context;


    private VolleySingleton(Context context) {
        this.context = context;
        this.request = getRequestQueue();
    }




    public static synchronized VolleySingleton getInstaceVolley (Context context){
        if (instanceVolley == null){
            instanceVolley = new VolleySingleton(context);
        }

        return instanceVolley;
    }


    public RequestQueue getRequestQueue() {
        if (request == null){

            request = Volley.newRequestQueue(context.getApplicationContext());
        }

        return request;
    }


    public <T> void addToRequestQueue (Request<T> request){
        getRequestQueue().add(request);
    }

}
