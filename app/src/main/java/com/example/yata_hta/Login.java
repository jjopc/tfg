package com.example.yata_hta;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.yata_hta.temporary.ChatMessage;
import com.example.yata_hta.utilities.JWTUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class Login extends AppCompatActivity {

    private Button login;
    private Button register;
    private EditText userName;
    private EditText pwd;
    private CheckBox remember;
    private Context context;
    String decodeAccess;
    private SharedPreferences preferences;
    SharedPreferences.Editor editor;

    private String access = "";
    private String refresh = "";

    String URL = "https://yatahta.juanjodev.es/auth/login/";
    String URL_REFRESH = "https://yatahta.juanjodev.es/auth/login/refresh/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initElements();

    }



    private void checkSessionDataBase(String user, String pws){


        //comprobar el login medainte un post a la API
        JSONObject param = new JSONObject();
        try {

            param.put("username",user);
            param.put("password",pws);


        } catch (JSONException e) {
            e.printStackTrace();
        }


        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL, param,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String access = response.getString("access");
                            String refresh= response.getString("refresh");
                            editor.putString("access", access);
                            editor.putString("refresh", refresh);
                            editor.apply();

                            // si el boton check esta up --> guardamos session
                            if (remember.isChecked()){
                                saveSession();
                            }

                            try {
                                decodeAccess = JWTUtils.decoded(access);
                                int userId = JWTUtils.stringToJSONUserID(decodeAccess);
                                editor.putInt("userID", userId);
                                editor.putString("userName", user);
                                editor.apply();

                            }catch (Exception e){
                                e.printStackTrace();
                            }

                            Boolean firstTime = preferences.getBoolean("firstTime",true);
                            if(firstTime){
                                editor.putBoolean("firstTime",false);
                                editor.apply();
                                startActivity(new Intent(context, StartForm.class));

                            }else{
                                startActivity(new Intent(context, User.class));
                            }
             } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(error instanceof ServerError){
                            Log.d("TAG","Error en servidor");
                        }
                        if(error instanceof NoConnectionError){//no conexion internet
                            Log.d("TAG","No hay conexion a internet");
                        }

                        if(error instanceof NetworkError){//desconexionsocket
                            Log.d("TAG","NetWorkError");
                        }

                    }
                });
        VolleySingleton.getInstaceVolley(getApplicationContext()).addToRequestQueue(request);
    }


    private void closeSession(){
        try {
            editor.putBoolean("checked",false);
            editor.apply();
        }catch (Exception w){
            Log.d("TAG","Error en checked = false en SharePreferences");
        }
    }
    private void saveSession(){
        // guardamos que se ah clickado en recordarme

        try {
            editor.putBoolean("checked",true);
            editor.apply();
        }catch (Exception w){
            Log.d("TAG","Error en guardar datos en SharePreferences");
        }

    }

    private void newToken (){
        JSONObject param = new JSONObject();
        try {
            refresh = preferences.getString("refresh","");
            param.put("refresh",refresh);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL_REFRESH, param,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String access = response.getString("access");
                            String refresh= response.getString("refresh");
                            editor.putString("access", access);
                            editor.putString("refresh", refresh);
                            editor.apply();
                            closeSession();
                            startActivity(new Intent(context, Login.class));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(error instanceof ServerError){
                            Log.d("TAG","Error en servidor");
                        }
                        if(error instanceof NoConnectionError){//no conexion internet
                            Log.d("TAG","No hay conexion a internet");
                        }
                        if(error instanceof NetworkError){//desconexionsocket
                            Log.d("TAG","NetWorkError");
                        }
                    }
                });
        VolleySingleton.getInstaceVolley(getApplicationContext()).addToRequestQueue(request);
    }


    private void initElements(){

        preferences= this.getSharedPreferences("LOGIN",this.MODE_PRIVATE);
        editor = preferences.edit();
        context = this;

        login = findViewById(R.id.log_in_button);
        register = findViewById(R.id.sign_in_button);
        userName = findViewById(R.id.userName);
        pwd = findViewById(R.id.password);
        remember = findViewById(R.id.recordar);



        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String user = userName.getText().toString();
                String password = pwd.getText().toString();

                //Comprobar si los datos son correcots o estan vacios
                if (user.equals("") || password.equals("")) {
                    Toast.makeText(context, " Introduzca los datos", Toast.LENGTH_LONG).show();
                }else{
                    // Validar datos con la BBDD
                    checkSessionDataBase(user, password);//Si los datos concuerdan con la BBDD
                }

            }
        });

        //editor.putString("access", "");
        //editor.putString("refresh", "");

        access= preferences.getString("access","");




        if (!access.equals("")){
            decodeAccess = JWTUtils.decoded(access);
            // Si ha expirado hay que hacer una peticion con el Token refresh
            if(JWTUtils.isAccessTokenExpired(decodeAccess)){
                newToken();
            }
        }






        Boolean loginUP = preferences.getBoolean("checked",false);

        if(loginUP){

            //Comrpobar IDMedico
            String medicalcode = preferences.getString("doctorID","");
            if(medicalcode.equals("")){
                getMedicalID();
            }
            else{
                Intent a = new Intent(context, User.class);
                startActivity(a);
            }

        }





    }

    private void getMedicalID(){
        String URL_MEDICO = "https://yatahta.juanjodev.es/api/users/";
        String user  = String.valueOf(preferences.getInt("userID",0));
        URL_MEDICO+=user +"/";
        JsonObjectRequest request= new JsonObjectRequest(Request.Method.GET, URL_MEDICO, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                                editor.putString("doctorID",response.getString("doctor"));
                                editor.apply();
                                Intent a = new Intent(context, User.class);
                                startActivity(a);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(error instanceof ServerError){
                            Log.d("TAG","Error en servidor");
                        }
                        if(error instanceof NoConnectionError){//no conexion internet
                            Log.d("TAG","No hay conexion a internet");
                        }

                        if(error instanceof NetworkError){//desconexionsocket
                            Log.d("TAG","NetWorkError");
                        }

                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer" + " " + access);//App.getToken()
                return headers;
            }
        };


        VolleySingleton.getInstaceVolley(getApplicationContext()).addToRequestQueue(request);

    }



    public void register(View view){
        Intent a = new Intent(this, Register.class);
        startActivity(a);
    }

}