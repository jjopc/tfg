package com.example.yata_hta;

import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;

import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TableLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import com.example.yata_hta.temporary.Drug;
import com.example.yata_hta.temporary.Pressure;
import com.example.yata_hta.utilities.TablePreassures;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Pressures extends AppCompatActivity {
    FloatingActionButton fab;
    BottomNavigationView navigation;

    String URL = "https://yatahta.juanjodev.es/api/users/";

    BarChart mpBarChart;
    LineChart lnChart;
    private TableLayout tableLayout;
    private List<Pressure> datos;
    private Context context;

    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    String userID;
    String token = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preassure);
        init();
    }

    @Override
    protected void onResume () {
        super.onResume();
       getPreassuresAPI();
    }
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(context, User.class);
        startActivity(intent);
        finish();
    }



    private void init(){

        //SharePrefereces
        preferences= this.getSharedPreferences("LOGIN",this.MODE_PRIVATE);
        editor = preferences.edit();
        token = preferences.getString("access" ,"");
        userID = String.valueOf(preferences.getInt("userID",0));
        URL += userID + "/pressures/" ;

        context = this;
        datos = new ArrayList<>();
        fab = findViewById(R.id.new_tension_form);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(context, PressureForm.class);
                startActivity(a);
            }
        });


        navigation = findViewById(R.id.bottom_navigation);
        navigation.setSelectedItemId(R.id.pressure);
        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.pressure:
                        return true;
                    case R.id.medication:
                        startActivity(new Intent(getApplicationContext(), DrugsMainView.class));
                        overridePendingTransition(0, 0);
                        return true;
                    case R.id.user:
                        startActivity(new Intent(getApplicationContext(), User.class));
                        overridePendingTransition(0, 0);
                        return true;
                    case R.id.mail:
                        startActivity(new Intent(getApplicationContext(), Chat.class));
                        overridePendingTransition(0, 0);
                        return true;

                }
                return false;
            }
        });
    }

    private void getPreassuresAPI() {
        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, URL, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {


                            int size = response.length();
                            datos.clear();
                            for ( int i = 0; i< size;i++){
                                JSONObject presion = response.getJSONObject(i);
                                float sys = (float)presion.getDouble("systolic");
                                float dias = (float)presion.getDouble("diastolic");
                                int puls = presion.getInt("bpm");
                                String arm = presion.getString("arm");
                                String fecha = presion.getString("date");
                                Pressure pres = new Pressure(sys,dias,puls,arm,null,fecha);
                                datos.add(pres);

                            }
                            crateTableLayout();
                            createChartBar();


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(error instanceof ServerError){
                            Log.d("TAG","Error en servidor");
                        }
                        if(error instanceof NoConnectionError){//no conexion internet
                            Log.d("TAG","No hay conexion a internet");
                        }

                        if(error instanceof NetworkError){//desconexionsocket
                            Log.d("TAG","NetWorkError");
                        }

                    }
                }){

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> headers = new HashMap<>();
                        // String credentials = "Dinna:12345678";
                        // String auth = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                        // headers.put("Authorization", auth);
                        headers.put("Authorization", "Bearer" + " " + token);//App.getToken()

                        return headers;
                    }
                 };

        VolleySingleton.getInstaceVolley(getApplicationContext()).addToRequestQueue(request);
    }





    private void crateTableLayout() {
        tableLayout = (TableLayout) findViewById(R.id.table);
        TablePreassures tablePress = new TablePreassures(tableLayout, getApplicationContext());
        tablePress.createHeader();
        tablePress.addData(datos);
        tablePress.backGroundHeader(R.color.preassureTable_header);

    }


    private void createChartBar() {
        lnChart = findViewById(R.id.chart);



        LineDataSet dataSetSystolic = new LineDataSet(systolic(), "Sistólica");
        dataSetSystolic.setColor(Color.RED);

        LineDataSet dataSetDiastolic = new LineDataSet(diastolic(), "Diastólica");
        dataSetDiastolic.setColor(Color.BLUE);

        LineDataSet dataSetHeartbeats = new LineDataSet(heart(), "Pulsaciones");
        dataSetHeartbeats.setColor(Color.GREEN);



        LineData data = new LineData(dataSetSystolic, dataSetDiastolic, dataSetHeartbeats);
        lnChart.setData(data);
        lnChart.setDragEnabled(true);//puedes mover las graficas con el dedo
        lnChart.setDrawBorders(true);



        ArrayList<String> listDate = new ArrayList<>();
        int icont = 0;
        for (Pressure i: datos  ) {
            listDate.add("");
            icont++;
        }
        XAxis xAxis = lnChart.getXAxis();
        xAxis.setValueFormatter(new IndexAxisValueFormatter(listDate));
//        lnChart.setVisibleXRangeMaximum(3);
        float barSpace = 0.08f;
        float groupSpace = 0.44f;

        lnChart.invalidate();

    }

    // Crea las barras systolicas

    private ArrayList<Entry> systolic() {
        ArrayList<Entry> sistolicList = new ArrayList<>();
        int cont = 0;

        for (int i = datos.size()-1 ; i >=0 ; i--) {
            sistolicList.add(new Entry(cont, datos.get(i).getSystolic()));
            cont++;
        }
//        for (Pressure i : datos) {
//            sistolicList.add(new Entry(cont, i.getSystolic()));
//            cont++;
//        }

        return sistolicList;
    }




    // Crea las barras disatoicas
    private ArrayList<Entry> diastolic() {
        ArrayList<Entry> diastolicList = new ArrayList<>();
        int cont = 0;
        for (int i = datos.size()-1 ; i >=0 ; i--) {
            diastolicList.add(new Entry(cont, datos.get(i).getDiastolic()));
            cont++;
        }
//        for (Pressure i : datos) {
//            diastolicList.add(new Entry(cont, i.getDiastolic()));
//            cont++;
//        }

        return diastolicList;
    }


    // Crea las barras de latidos
    private ArrayList<Entry> heart() {
        ArrayList<Entry> heartList = new ArrayList<>();
        int cont = 0;
        for (int i = datos.size()-1 ; i >=0 ; i--) {
            heartList.add(new Entry(cont, datos.get(i).getHeartbeats()));
            cont++;
        }
//        for (Pressure i : datos) {
//            heartList.add(new Entry(cont, i.getHeartbeats()));
//            cont++;
//        }


        return heartList;
    }




}
/*
    private void createChartBar() {
        mpBarChart = findViewById(R.id.chart);
        //mostrar graficas
        AppDataBase db = AppDataBase.getInstance(getApplicationContext());
        presDao = db.pressureDao();
        datos=  presDao.getAll();

        BarDataSet dataSetSystolic = new BarDataSet(systolic(), "systolic");
        dataSetSystolic.setColor(Color.RED);

        BarDataSet dataSetDiastolic = new BarDataSet(diastolic(), "diastolic");
        dataSetDiastolic.setColor(Color.BLUE);

        BarDataSet dataSetHeartbeats = new BarDataSet(heart(), "Heartbeats");
        dataSetHeartbeats.setColor(Color.GREEN);

        BarData data= new BarData(dataSetSystolic, dataSetDiastolic, dataSetHeartbeats);
        mpBarChart.setData(data);
        mpBarChart.setDragEnabled(true);//puedes mover las graficas con el dedo
        mpBarChart.setVisibleXRangeMaximum(3);
        float barSpace = 0.08f;
        float groupSpace = 0.44f;
        data.setBarWidth(0.10f);

        mpBarChart.getXAxis().setAxisMinimum(0);
        mpBarChart.getXAxis().setAxisMaximum(0+mpBarChart.getBarData().getGroupWidth(groupSpace,barSpace)*datos.size());
        mpBarChart.getAxisLeft().setAxisMinimum(0);

        mpBarChart.groupBars(0,groupSpace,barSpace);
        mpBarChart.invalidate();
    }


    // Crea las barras systolicas
    private ArrayList<BarEntry> systolic(){
            ArrayList<BarEntry> sistolicList = new ArrayList<>();
            int cont =0;
            for(PressureEntity i: datos){
                sistolicList.add(new BarEntry(cont, i.getSystolic()));
            }

            return sistolicList;
    }

    // Crea las barras disatoicas
    private ArrayList<BarEntry> diastolic(){
        ArrayList<BarEntry> diastolicList = new ArrayList<>();
        int cont =0;
        for(PressureEntity i: datos){
            diastolicList.add(new BarEntry(cont, i.getDiastolic()));
        }

        return diastolicList;
    }


    // Crea las barras de latidos
    private ArrayList<BarEntry> heart(){
        ArrayList<BarEntry> heartList = new ArrayList<>();
        int cont =0;
        for(PressureEntity i: datos){
            heartList.add(new BarEntry(cont, i.getHeartbeats()));
        }

        return heartList;
    }
}

/*
        AppDataBase db = AppDataBase.getInstance(this.getApplicationContext());
        PressureDAO presDao = db.pressureDao();
        PressureEntity pres = new PressureEntity(85,110,68,"left",null);
        presDao.insert(pres);

        Log.d("Presion prueba:", "Id=" + pres.getPressureId()+
                                           "Systolic=" + pres.getSystolic()+
                                           "diastolic=" + pres.getDiastolic()+
                                            "Heartbeats="+ pres.getHeartbeats()+
                                            "Arm=" +pres.getArm());*/