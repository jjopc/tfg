package com.example.yata_hta;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.RadioButton;
import android.view.View;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class BattleForm_1 extends AppCompatActivity {

    private RadioButton si1;
    private RadioButton no1;
    private RadioButton si2;
    private RadioButton no2;
    private Spinner spinner1;
    private Spinner spinner2;
    private Button next;
    private Context context;
    private String userID;
    private String token = "";

    private static final String URL = "https://yatahta.juanjodev.es/api/batles/";


    private SharedPreferences preferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_battle_form_1);
        init();
    }

    private void init() {
        si1 = (RadioButton) findViewById(R.id.si1);
        no1 = (RadioButton) findViewById(R.id.no1);
        si2 = (RadioButton) findViewById(R.id.si2);
        no2 = (RadioButton) findViewById(R.id.no2);
        spinner1 = findViewById(R.id.spinner1);
        spinner2 = findViewById(R.id.spinner2);
        next = findViewById(R.id.next);
        context = this;


        preferences= this.getSharedPreferences("LOGIN",this.MODE_PRIVATE);
        token = preferences.getString("access" ,"");
//        editor = preferences.edit();

        userID = String.valueOf(preferences.getInt("userID",0));


        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if((si1.isChecked()|| no1.isChecked()) && (si2.isChecked()|| no2.isChecked()) ){
                    pushBatleForm();
                }else{

                    Toast.makeText(context, "Seleccione todos las preguntas",Toast.LENGTH_LONG).show();
                }

            }
        });
    }





    private void pushBatleForm(){

        JSONObject param = new JSONObject();
        try {
            if(si1.isChecked()){
                param.put("answer1",true);
            }else{
                param.put("answer1",false);
            }


            if(si2.isChecked()){
                param.put("answer2",true);
            }else{
                param.put("answer2",false);
            }

            String asw3= spinner1.getSelectedItem().toString();
            String asw4= spinner2.getSelectedItem().toString();
//            param.put("answer3",spinner1.getSelectedItem().toString());
//            param.put("answer4",spinner2.getSelectedItem().toString());
            switch (asw3){
                case "Corazón": asw3 = "HT";break;
                case "Pulmones": asw3 = "LG";break;
                case "Estómago": asw3 = "ST";break;
                case "Riñones": asw3 = "KD";break;
                case "Pancreas": asw3 = "PC";break;
                case "Hígado": asw3 = "LV";break;
                case "Vista": asw3 = "VW";break;
                case "Oído": asw3 = "HR";break;
            }

            switch (asw4){
                case "Corazón": asw4 = "HT";break;
                case "Pulmones": asw4 = "LG";break;
                case "Estómago": asw4 = "ST";break;
                case "Riñones": asw4 = "KD";break;
                case "Pancreas": asw4 = "PC";break;
                case "Hígado": asw4 = "LV";break;
                case "Vista": asw4 = "VW";break;
                case "Oído": asw4 = "HR";break;
            }

            param.put("answer3",asw3);
            param.put("answer4",asw4);
            param.put("isOk", true);
            param.put("user",userID);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL, param,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                       Intent intent = new Intent(context, User.class);
                       startActivity(intent);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(error instanceof ServerError){
                            Log.d("TAG","Error en servidor");
                        }
                        if(error instanceof NoConnectionError){//no conexion internet
                            Log.d("TAG","No hay conexion a internet");
                        }
                        if(error instanceof NetworkError){//desconexionsocket
                            Log.d("TAG","NetWorkError");
                        }
                    }
                }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer" + " " + token);//App.getToken()
                return headers;
            }
        };

        VolleySingleton.getInstaceVolley(getApplicationContext()).addToRequestQueue(request);
    }
}