package com.example.yata_hta.recyclerViewAdapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.yata_hta.databinding.ItemContainerReceiverdMessageBinding;
import com.example.yata_hta.databinding.ItemContainerSentMessageBinding;

import com.example.yata_hta.temporary.ChatMessage;

import java.util.ArrayList;
import java.util.List;

public class ChatRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private  List<ChatMessage> chatMessages = new ArrayList<ChatMessage>();;
    private Context context;
    private  Bitmap reciverProfileImage = null;
    private final String senderId;// emisor == > usuario 9
    public static final int VIEW_TYPE_SENT = 1;
    public static final int VIEW_TYPE_RECEIVED = 2;


    public ChatRecyclerViewAdapter(List<ChatMessage> chatMessages, Bitmap reciverProfileImage, String senderId) {
        this.chatMessages = chatMessages;
        this.reciverProfileImage = reciverProfileImage;
        this.senderId = senderId;
    }

    public ChatRecyclerViewAdapter(String senderId) {
        this.senderId = senderId;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder( ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_SENT){
            return new SentMessageViewHolder(ItemContainerSentMessageBinding.inflate(LayoutInflater.from(parent.getContext()),parent,false));
        }else{
            return new RecivedMessageViewHolder(ItemContainerReceiverdMessageBinding.inflate(LayoutInflater.from(parent.getContext()),parent,false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if(getItemViewType(position) == VIEW_TYPE_SENT){
            ((SentMessageViewHolder)holder).setData(chatMessages.get(position));
        }else{
            ((RecivedMessageViewHolder)holder).setData(chatMessages.get(position));
        }
    }

    @Override
    public int getItemCount() {
        return chatMessages.size();
    }

    @Override
    public int getItemViewType(int position) {
        if(chatMessages.get(position).getSenderId().equals(senderId)){
            return VIEW_TYPE_SENT;
        }else{
            return VIEW_TYPE_RECEIVED;
        }
    }

    public void setChat (List<ChatMessage> ch){
        this.chatMessages = ch;
        notifyDataSetChanged();
    }

    static class SentMessageViewHolder extends RecyclerView.ViewHolder {

       private  final ItemContainerSentMessageBinding binding;

       SentMessageViewHolder(ItemContainerSentMessageBinding item){
           super(item.getRoot());
           binding = item;
       }

       void setData (ChatMessage chatMessage){
           binding.textMessage.setText(chatMessage.getMessage());
           binding.textDataTime.setText(chatMessage.getDataTime());
       }

   }



    static  class RecivedMessageViewHolder extends RecyclerView.ViewHolder{
        private final ItemContainerReceiverdMessageBinding binding;

        RecivedMessageViewHolder(ItemContainerReceiverdMessageBinding item){
            super(item.getRoot());
            binding = item;
        }

        void setData(ChatMessage chatmessage /*Bitmap reciverProfileImage*/){
            binding.textMessage.setText(chatmessage.getMessage());
            binding.textDataTime.setText((chatmessage.getDataTime()));
          //  binding.imageProfile.setImageBitmap(reciverProfileImage);

        }
    }

}
