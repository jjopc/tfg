package com.example.yata_hta.recyclerViewAdapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.yata_hta.DrugListPoints;
import com.example.yata_hta.R;
import com.example.yata_hta.SearchDrug;
import com.example.yata_hta.User;
import com.example.yata_hta.VolleySingleton;
import com.example.yata_hta.temporary.Drug;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DrugPointsAdapter extends RecyclerView.Adapter<DrugPointsAdapter.ViewHolder> {
    private Context context;
    private int points;

    public  boolean[] chechedItemList ;
    private List<Drug> drugs = new ArrayList<>();
    private String nameOfEat;

    public DrugPointsAdapter(Context context, String name) {
        this.context = context;
        this.nameOfEat = name;
        chechedItemList = new boolean[50];
    }

    @NonNull
    @Override
    public DrugPointsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_search_drug, parent, false);
        DrugPointsAdapter.ViewHolder holder = new DrugPointsAdapter.ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull DrugPointsAdapter.ViewHolder holder, int position) {
        holder.asignarDatos(drugs.get(position), position);
    }

    @Override
    public int getItemCount() {
        return drugs.size();
    }

    public void setListDrus(List<Drug> listDrus){
        this.drugs = listDrus;
        notifyDataSetChanged();
    }



    public boolean[] getChechedItemList(){return chechedItemList;}

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private TextView drugName;
        private CheckBox checkBox;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            drugName = itemView.findViewById(R.id.drug);
            checkBox = itemView.findViewById(R.id.checkbox);
        }

        public void asignarDatos(Drug s, int pos) {
            drugName.setText(s.getDrugName());
            if(s.isAt_breakfast_taken() && nameOfEat.equals("breakfast")){
                checkBox.setChecked(true);
                checkBox.setEnabled(false);
            }

            if(s.isAt_lunch_taken()&& nameOfEat.equals("lunch")){
                checkBox.setChecked(true);
                checkBox.setEnabled(false);
            }

            if(s.isAt_dinner_taken()&& nameOfEat.equals("dinner")){
                checkBox.setChecked(true);
                checkBox.setEnabled(false);
            }
            if(s.isAt_bedtime_taken()&& nameOfEat.equals("bed")){
                checkBox.setChecked(true);
                checkBox.setEnabled(false);
            }

            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (checkBox.isChecked()){
                        chechedItemList[pos] =true;
                        points++;
                    }

                    if(!checkBox.isChecked()){
                        chechedItemList[pos] =false;
                        points--;
                    }
                }
            });
//            checkBox.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (checkBox.isChecked()){
//                        points++;
//                    }
//
//                    if(!checkBox.unc()){
//                        points--;
//                    }
//                }
//            });

        }
    }



//
//    private void sendToPatchFielTaken (int idDrug){
//        String URL = "https://yatahta.juanjodev.es/api/posologies/";
//        URL+= String.valueOf(idDrug);
//        JSONObject param = new JSONObject();
////    try {
////
////
////    } catch (JSONException e) {
////        e.printStackTrace();
////    }
//
//        JsonObjectRequest request = new JsonObjectRequest(Request.Method.PATCH, URL, param,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        Intent intent = new Intent(context, User.class);
//                        startActivity(intent);
//                    }
//                },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        if(error instanceof ServerError){
//                            Log.d("TAG",error.toString());
//                        }
//                        if(error instanceof NoConnectionError){//no conexion internet
//                            Log.d("TAG","No hay conexion a internet");
//                        }
//                        if(error instanceof NetworkError){//desconexionsocket
//                            Log.d("TAG","NetWorkError");
//                        }
//                    }
//                }){
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                Map<String, String> headers = new HashMap<>();
//                headers.put("Authorization", "Bearer" + " " + token);//App.getToken()
//                return headers;
//            }
//        };
//        VolleySingleton.getInstaceVolley(getApplicationContext()).addToRequestQueue(request);
//
//    }
}
