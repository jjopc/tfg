package com.example.yata_hta.recyclerViewAdapters;

import android.content.Context;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.yata_hta.DrugsMainView;
import com.example.yata_hta.Pressures;
import com.example.yata_hta.R;

import com.example.yata_hta.VolleySingleton;
import com.example.yata_hta.temporary.Drug;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DrugRecyclerViewAdapter extends RecyclerView.Adapter<DrugRecyclerViewAdapter.ViewHolder> {
    private static final String TAG = DrugRecyclerViewAdapter.class.getSimpleName();
    private List<Drug> drugEntityList = new ArrayList<>();
    private Context context;
    String URL = "https://yatahta.juanjodev.es/api/drugs/";
    String URL_DRUGS = "https://yatahta.juanjodev.es/api/users/";
    String URL_POSOLOGIAS = "https://yatahta.juanjodev.es/api/posologies/";
    private String token;
    private OnNoteListener mOnNoteListener;


    public DrugRecyclerViewAdapter(OnNoteListener onNoteListener, Context context, String token) {
        this.context = context;
        this.token = token;
        this.mOnNoteListener = onNoteListener;
    }

    @NonNull
    @Override
    public DrugRecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.drug_recycler_view_item, parent, false);
        ViewHolder holder = new ViewHolder(view,mOnNoteListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.asignarDatos(drugEntityList.get(position), position);
    }

    @Override
    public int getItemCount() {
        return drugEntityList.size();
    }

    public void setDrugEntityList(List<Drug> drugEntityList) {
        this.drugEntityList = drugEntityList;
        notifyDataSetChanged(); // En caso de cambiar la lista de medicamentos, este método notifica a la vista
    }

    // Se llama así por convenio, es la clase responsable de la vista de cada item del RecyclerView
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        private TextView drugName;
        private AppCompatImageView delete;
        private TextView breakfast;
        private TextView lunch;
        private TextView dinner;
        private TextView bed;
        OnNoteListener onNoteListener;
        // Aquí se añaden el resto de elementos que queramos del item-layout

        public ViewHolder(@NonNull View itemView, OnNoteListener onNoteListener) {
            super(itemView);
            drugName = itemView.findViewById(R.id.drug_name_recycler_view_item);
            delete = itemView.findViewById(R.id.drug_delete_button_recycler_view_item);
            breakfast = itemView.findViewById(R.id.breakfastActived);
            lunch = itemView.findViewById(R.id.lunchActived);
            dinner = itemView.findViewById(R.id.DinerActived);
            bed = itemView.findViewById(R.id.sleepActived);
            this.onNoteListener = onNoteListener;
            itemView.setOnClickListener(this);
        }

        public TextView getDrugName() {
            return drugName;
        }

        public void asignarDatos(Drug drug, int pos) {

            drugName.setText(drug.getDrugName());


            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {



                    AlertDialog.Builder alert = new AlertDialog.Builder(context);
                    alert.setMessage("¿Desea eliminar el medicamento?")
                            .setCancelable(false)
                            .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    // Hacer push a base de datos de delete
                                    String URLID = URL_POSOLOGIAS;
                                    URLID += drug.getId() + "/";

                                    JsonObjectRequest request = new JsonObjectRequest(Request.Method.DELETE, URLID, null,
                                            new Response.Listener<JSONObject>() {
                                                @Override
                                                public void onResponse(JSONObject response) {
                                                    int i = 0;
                                                }
                                            },
                                            new Response.ErrorListener() {
                                                @Override
                                                public void onErrorResponse(VolleyError error) {
                                                    if (error instanceof ServerError) {
                                                        Log.d("TAG", "Error en servidor");
                                                    }
                                                    if (error instanceof NoConnectionError) {//no conexion internet
                                                        Log.d("TAG", "No hay conexion a internet");
                                                    }

                                                    if (error instanceof NetworkError) {//desconexionsocket
                                                        Log.d("TAG", "NetWorkError");
                                                    }

                                                }
                                            }) {
                                        @Override
                                        public Map<String, String> getHeaders() throws AuthFailureError {
                                            Map<String, String> headers = new HashMap<>();
                                            headers.put("Authorization", "Bearer" + " " + token);//App.getToken()
                                            return headers;
                                        }
                                    };

                                    VolleySingleton.getInstaceVolley(context).addToRequestQueue(request);

                                    drugEntityList.remove(pos);
                                    setDrugEntityList(drugEntityList);
                                }
                            })
                            .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });

                    AlertDialog titulo = alert.create();
                    titulo.setTitle("Eliminar medicamento");
                    titulo.show();



                }
            });

            if (drug.getAt_breakfast().equals("null")) {
                breakfast.setText("0");
            } else {
                breakfast.setText(drug.getAt_breakfast());
            }

            if (drug.getAt_lunch().equals("null")) {
                lunch.setText("0");
            } else {
                lunch.setText(drug.getAt_lunch());
            }

            if (drug.getAt_dinner().equals("null")) {
                dinner.setText("0");
            } else {
                dinner.setText(drug.getAt_dinner());
            }

            if (drug.getAt_bedtime().equals("null")) {
                bed.setText("0");
            } else {
                bed.setText(drug.getAt_bedtime());
            }

        }


        @Override
        public void onClick(View v) {
            onNoteListener.onNoteClick(getAdapterPosition());
        }
    }

    public interface OnNoteListener {
        void onNoteClick(int position);
    }
}
