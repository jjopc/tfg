package com.example.yata_hta.recyclerViewAdapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.yata_hta.R;
import com.example.yata_hta.SearchDrug;

import java.util.ArrayList;
import java.util.List;

public class SearchDrugsAdapter extends RecyclerView.Adapter<SearchDrugsAdapter.ViewHolder>{
    private Context context;
   // private String nameDrug;
    private List<String> drugs = new ArrayList<>();

    public SearchDrugsAdapter(Context context) {
        this.context = context;
       //this.nameDrug = nameDrug;
       // this.drugs = drugs;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_search_drug, parent, false);
        SearchDrugsAdapter.ViewHolder holder = new SearchDrugsAdapter.ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.asignarDatos(drugs.get(position));
    }

    @Override
    public int getItemCount() {
        return drugs.size();
    }

    public void setListDrus(List<String> listDrus){
        this.drugs = listDrus;
        notifyDataSetChanged();
    }


    public class ViewHolder extends RecyclerView.ViewHolder{

        private TextView drugName;
        private CheckBox checkBox;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            drugName = itemView.findViewById(R.id.drug);
            checkBox = itemView.findViewById(R.id.checkbox);
        }

        public void asignarDatos(String s) {
            drugName.setText(s);
            checkBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (checkBox.isChecked()){
                        SearchDrug.drugName = s;
                    }
                }
            });

        }
    }
}
