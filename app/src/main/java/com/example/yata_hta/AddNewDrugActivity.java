package com.example.yata_hta;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.yata_hta.temporary.Drug;
import com.example.yata_hta.utilities.AlarmReceiver;
import com.example.yata_hta.utilities.DosageUnits;
import com.example.yata_hta.utilities.Frequency;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ca.antonious.materialdaypicker.MaterialDayPicker;

public class AddNewDrugActivity extends AppCompatActivity {
    private static final String TAG = AddNewDrugActivity.class.getSimpleName();
    private final int DRUG_SEARCH = 1;
    private EditText drugName;
    private  EditText drugDosage;
    private CheckBox breakfast;
    private CheckBox lunch;
    private CheckBox dinner;
    private CheckBox bed;
    private EditText breakfastDoss;
    private EditText lunchDoss;
    private EditText dinnerDoss;
    private EditText bedDoss;
    private TextView initDate;
    private TextView endDate;
    private AppCompatImageView initPiker;
    private AppCompatImageView endPicker;
    private AppCompatImageView search;
    private AppCompatImageView moreOptions;
    private EditText dossageFrec;
    private EditText freq;
    private Spinner dosageSpinner;
    private Spinner frequencySpinner;
    private Button btnAccept;
    private  LinearLayout linearLayout;
    private  LinearLayout linearLayoutMoreOPtions;
    private String nameDrug;

    //private static String drugID;


    private Button btnDiscard;
    private Button notifiacion;

    private String URL_DRUGS = "https://yatahta.juanjodev.es/api/drugs/";
    private String URL_POSOLOGIES = "https://yatahta.juanjodev.es/api/posologies/";
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    String token = "";
    String userID;


    // Manager de las notifiacaciones
    private NotificationManager mNotifyManager;

    // Notification ID.
    // Creamos constantes para los ID de la notificaion y para el canal de notificaciones
    private static final int NOTIFICATION_ID = 0;
    private static final String PRIMARY_CHANNEL_ID = "primary_notification_channel";


    private static final String ACTION_UPDATE_NOTIFICATION = "com.example.android.notifyme.ACTION_UPDATE_NOTIFICATION";
    private NotificationReceiver mReceiver;


    private AlarmManager alarmMgr;
    private PendingIntent alarmIntent;

    private List<Drug> drugs;
    private Context context;

    // TODO no hemos contemplado la toma de medicamentos en quincenas o varios días al mes.

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_drug);
        init();
/*
        frequencySpinner.setOnItemSelectedListener(new FrequencySpinner());
        frequencySpinner.getSelectedItemId();


        // llamada para crear el canal de notificacion
        createNotificationChannel();
;*/
    }
    @Override
    protected void onDestroy() {
        unregisterReceiver(mReceiver);
        super.onDestroy();
    }

    private void init(){

        //SharePrefereces
        preferences= this.getSharedPreferences("LOGIN",this.MODE_PRIVATE);
        editor = preferences.edit();
        token = preferences.getString("access" ,"");
        userID = String.valueOf(preferences.getInt("userID",0));

        // components XML
        drugName = findViewById(R.id.drugNameEditText);
        drugDosage = findViewById(R.id.drugDosageEditText);
        breakfast = findViewById(R.id.desayunoCheckBox);
        lunch = findViewById(R.id.comidaCheckBox);
        dinner = findViewById(R.id.cenaCheckBox);
        bed = findViewById(R.id.dormirCheckBox);
        breakfastDoss = findViewById(R.id.desayunoDosis);
        lunchDoss = findViewById(R.id.comidaDosis);
        dinnerDoss = findViewById(R.id.cenaDosis);
        bedDoss = findViewById(R.id.dormirDosis);
        initDate = findViewById(R.id.initDate);
        endDate = findViewById(R.id.endDate);
        moreOptions = findViewById(R.id.moreOption);
        initPiker = findViewById(R.id.diaInicio);
        endPicker = findViewById(R.id.diaFin);
        search = findViewById(R.id.search);
        dosageSpinner = findViewById(R.id.drugDosageSpinner);
        frequencySpinner = findViewById(R.id.frequencySpinner);
        dossageFrec = findViewById(R.id.dosisFrecuencia);
        freq = findViewById(R.id.frecuencia);
        btnAccept = findViewById(R.id.accept_btn);
        nameDrug = "";

        linearLayout = findViewById(R.id.options);
        linearLayout.setVisibility(View.INVISIBLE);

        linearLayoutMoreOPtions = findViewById(R.id.otherOptions);
        linearLayoutMoreOPtions.setVisibility(View.INVISIBLE);

        context = this;


        //Listeners
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(drugName.getText().equals("")){
                    Toast.makeText(context, "Introduce un nombre del medicamento",Toast.LENGTH_LONG).show();
                }else{
                    Intent intent = new Intent(context, SearchDrug.class);
                    intent.putExtra("searchDrug",drugName.getText().toString());
                    startActivityForResult(intent,DRUG_SEARCH);
                }
            }
        });


        initPiker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar calendar = Calendar.getInstance();
                final int day = calendar.get(Calendar.DAY_OF_MONTH);
                final int month=  calendar.get(Calendar.MONTH);
                final int year = calendar.get(Calendar.YEAR);

                DatePickerDialog datePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int y, int m, int d) {
                        initDate.setText(d + "/" + (m+1) + "/" + y);
                    }
                },year,month,day);

                datePickerDialog.show();
            }
        });

        endPicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar calendar = Calendar.getInstance();
                final int day = calendar.get(Calendar.DAY_OF_MONTH);
                final int month=  calendar.get(Calendar.MONTH);
                final int year = calendar.get(Calendar.YEAR);

                DatePickerDialog datePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int y, int m, int d) {
                        endDate.setText(d + "/" + (m+1) + "/" + y);
                    }
                },year,month,day);

                datePickerDialog.show();

            }
        });

        moreOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linearLayout.setVisibility(View.VISIBLE);
            }
        });

        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {




                if (drugName.getText().toString().equals("")){
                    Toast.makeText(context, "Introduzca un medicamento",Toast.LENGTH_LONG).show();
                }else{


                    AlertDialog.Builder alert = new AlertDialog.Builder(context);
                    alert.setMessage("¿Desea agregar la mediación?")
                            .setCancelable(false)
                            .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    searchDrug();
                                }
                            })
                            .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });

                    AlertDialog titulo = alert.create();
                    titulo.setTitle("Añadir medicación");
                    titulo.show();

                }

            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == DRUG_SEARCH){
            if(resultCode == RESULT_OK){
                drugName.setText(data.getStringExtra("searchDrug"));
            }else if(resultCode == RESULT_CANCELED){
                Log.d("Error", "Error al buscar medicacion");
            }
        }
    }


    private  void searchDrug(){
        String name = drugName.getText().toString();
        String URL_SEARCH =  "https://yatahta.juanjodev.es/api/drugs/?name=" + name;

        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, URL_SEARCH, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        //   Toast.makeText(context, "Mensaje enviado",Toast.LENGTH_LONG).show();
                        //   Log.d("TAG","Success");
                        try {
                            int size = response.length();

                            if(response.length() == 0 ){// si NO esta en BD
                                int f;
                                addNewDrug();
                            }else{ // si ya esta en BD
                                JSONObject medicina = response.getJSONObject(0);
                                addNewPosologies(medicina);
                            }




                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(error instanceof ServerError){
                            Log.d("TAG","Error en servidor");
                        }
                        if(error instanceof NoConnectionError){//no conexion internet
                            Log.d("TAG","No hay conexion a internet");
                        }

                        if(error instanceof NetworkError){//desconexionsocket
                            Log.d("TAG","NetWorkError");
                        }

                    }
                }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer" + " " + token);//App.getToken()
                return headers;
            }
        };

        VolleySingleton.getInstaceVolley(getApplicationContext()).addToRequestQueue(request);
    }


    private void addNewDrug(){

        JSONObject param = new JSONObject();
        try {
            String name = drugName.getText().toString();
            double dosage = 0.0;
            try {
                dosage = Double.parseDouble(drugDosage.getText().toString());
            } catch (Exception e) {
                Log.d(TAG, "Problema parseFloat");
            }
            String dos = String.valueOf(dosage) + " " + dosageSpinner.getSelectedItem().toString();;

            param.put("name",name);
            param.put("dosage",dos);


        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL_DRUGS, param,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                     //   Toast.makeText(context, "Mensaje enviado",Toast.LENGTH_LONG).show();
                     //   Log.d("TAG","Success");


                            //String drugID = response.getString("id");
                            addNewPosologies(response);


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(error instanceof ServerError){
                            Log.d("TAG","Error en servidor");
                        }
                        if(error instanceof NoConnectionError){//no conexion internet
                            Log.d("TAG","No hay conexion a internet");
                        }

                        if(error instanceof NetworkError){//desconexionsocket
                            Log.d("TAG","NetWorkError");
                        }

                    }
                }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                //String credentials = "Dinna:12345678";
                //String auth = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                //headers.put("Authorization", auth);
                headers.put("Authorization", "Bearer" + " " + token);//App.getToken()
                return headers;
            }
        };

        VolleySingleton.getInstaceVolley(getApplicationContext()).addToRequestQueue(request);
    }



    private void addNewPosologies(JSONObject drug){

        JSONObject param = new JSONObject();
        try {


            //param.put("drug_name",drug.getString("name"));

            if (breakfast.isChecked()){
                if(!breakfastDoss.getText().equals("")){
                    param.put("at_breakfast",breakfastDoss.getText().toString());
                }else{
                    Toast.makeText(context, "Introduzca unidades en el Desayuno",Toast.LENGTH_LONG).show();
                }
            }else{
                param.put("at_breakfast",0);
            }

            if (lunch.isChecked()){
                if(!lunchDoss.getText().equals("")){
                    param.put("at_lunch",lunchDoss.getText().toString());
                }else{
                    Toast.makeText(context, "Introduzca unidades en el Comida",Toast.LENGTH_LONG).show();
                }
            }else{
                param.put("at_lunch",0);
            }

            if (dinner.isChecked()){
                if(!dinnerDoss.getText().equals("")){
                    param.put("at_dinner",dinnerDoss.getText().toString());
                }else{
                    Toast.makeText(context, "Introduzca unidades en el Cena",Toast.LENGTH_LONG).show();
                }
            }else{
                param.put("at_dinner",0);
            }

            if (bed.isChecked()){
                if(!bedDoss.getText().equals("")){
                    param.put("at_bedtime",bedDoss.getText().toString());
                }else{
                    Toast.makeText(context, "Introduzca unidades en la Cena",Toast.LENGTH_LONG).show();
                }
            }else{
                param.put("at_bedtime",0);
            }
            //param.put("start_time",1);
            if(!initDate.getText().equals("")){

                String oldDateString = initDate.getText().toString();
                String newDateString;

                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                try{
                    Date d = sdf.parse(oldDateString);
                    sdf.applyPattern("yyyy-MM-dd");
                    newDateString = sdf.format(d);
                    param.put("start_date",newDateString);
                }catch (Exception e){
                    Log.d("PARSE", "Error en parsear fecha");
                }



            }else {
                Toast.makeText(context, "Introduzca una fecha de inicio",Toast.LENGTH_LONG).show();
            }

            if(!endDate.getText().equals("")){

                String oldDateString = endDate.getText().toString();
                String newDateString;

                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                try{
                    Date d = sdf.parse(oldDateString);
                    sdf.applyPattern("yyyy-MM-dd");
                    newDateString = sdf.format(d);
                    param.put("end_date",newDateString);
                }catch (Exception e){
                    Log.d("PARSE", "Error en parsear fecha");
                }


            }else {
                Toast.makeText(context, "Introduzca una fecha de inicio",Toast.LENGTH_LONG).show();
            }
            /*
            List<String> week = Arrays.asList(new String[]{"MO,TU,WE"});
            List<Integer> month = Arrays.asList(new Integer[]{1,2,3,4});
            List<String> year = Arrays.asList(new String[]{"JA,FE,MA"});*/

            param.put("x_times",1);
            param.put("frequency","HR");
           // param.put("days_of_week",null);
            //param.put("days_of_month",null);
            //param.put("months_of_year",null);

            param.put("user",Integer.valueOf(userID));
            param.put("drug",Integer.valueOf(drug.getString("id")));

            Log.d("FER", param.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL_POSOLOGIES, param,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //   Toast.makeText(context, "Mensaje enviado",Toast.LENGTH_LONG).show();
                        //   Log.d("TAG","Success");

                            Toast.makeText(context, "Posología introducida corectamente",Toast.LENGTH_LONG).show();
                            Intent intent= new Intent(context, DrugsMainView.class);
                            startActivity(intent);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                        if(error instanceof ServerError){
                            Log.d("TAG","Error en servidor");
                        }
                        if(error instanceof NoConnectionError){//no conexion internet
                            Log.d("TAG","No hay conexion a internet");
                        }

                        if(error instanceof NetworkError){//desconexionsocket
                            Log.d("TAG","NetWorkError");
                        }

                    }
                }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                //String credentials = "Dinna:12345678";
                //String auth = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                //headers.put("Authorization", auth);
                headers.put("Authorization", "Bearer" + " " + token);//App.getToken()
                return headers;
            }
        };

        VolleySingleton.getInstaceVolley(getApplicationContext()).addToRequestQueue(request);
    }




    /**
     * Creates a Notification channel, for OREO and higher.
     */

    /*
    public void createNotificationChannel() {
        // Inicializamos el manager de las notificaiones
        mNotifyManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        // Notification channels are only available in OREO and higher.
        // So, add a check on SDK version.
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {

            // Create a NotificationChannel
            NotificationChannel notificationChannel = new NotificationChannel(PRIMARY_CHANNEL_ID, "Yahta_hta", NotificationManager.IMPORTANCE_HIGH);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.setDescription("Notificacion recordar pastillas");

            mNotifyManager.createNotificationChannel(notificationChannel);
        }
    }
    */

    /**
     * Helper method that builds the notification.
     *
     * @return NotificationCompat.Builder: notification build with all the
     * parameters.
     */



    public void discard(View view) {
        Intent replyIntent = new Intent();
        setResult(RESULT_CANCELED, replyIntent);
        finish();
    }

    /*
    private void init(){
        // Inicializo las vistas del layout
        drugNameEditText = findViewById(R.id.drugNameEditText);
        dosageEditText = findViewById(R.id.drugDosageEditText);
        dosageSpinner = findViewById(R.id.drugDosageSpinner);
        frequencySpinner = findViewById(R.id.frequencySpinner);
        xTimesADayEditText = findViewById(R.id.xTimesADayEditText);
        xTimesADaySpinner = findViewById(R.id.xTimesADaySpinner);

        xTimesADayTextView = findViewById(R.id.xTimesADayTextView);
        weekChooser = findViewById(R.id.weekdays_chooser);
        materialDayPicker = findViewById(R.id.day_picker);

        monthDayChooser = findViewById(R.id.month_day_chooser);
        monthlyDayPicker = findViewById(R.id.monthlyDayPicker);
        // Esto es para que sea compatible con todos los meses del año cuando establezcamos el recordatorio
        monthlyDayPicker.setMaxValue(28);
        monthlyDayPicker.setMinValue(1);

        btnAccept = findViewById(R.id.btnAccept);
        btnDiscard = findViewById(R.id.btnDiscard);
        notifiacion = findViewById(R.id.notificacion);
        mReceiver = new NotificationReceiver();

        // Inicializo el Manager de la Alarma

        alarmMgr = (AlarmManager) getSystemService(ALARM_SERVICE);
        notifiacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Creamos el intent apuntando al AlamrReciver
                Intent intentAlarm = new Intent(context, AlarmReceiver.class);
                // metemos ese Intent en un pending intent que se activara cuando la alarmse se active
                alarmIntent= PendingIntent.getBroadcast(context, NOTIFICATION_ID, intentAlarm, 0);
                // Creamos la alarma
                alarmMgr.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + 10*1000, alarmIntent);

                SharedPreferences preferences = getSharedPreferences("pruebaPuntos",Context.MODE_PRIVATE);
                int total = preferences.getInt("TotalPuntos",0);

            }

            
        });
        
    }*/



    public class NotificationReceiver extends BroadcastReceiver {

        public NotificationReceiver() { }

        @Override
        public void onReceive(Context context, Intent intent) {
            // Update the notification
            SharedPreferences preferences = getSharedPreferences("pruebaPuntos",Context.MODE_PRIVATE);
            int total = preferences.getInt("TotalPuntos",0);
            Log.d("puntos que lleva", String.valueOf(total));


            int puntos = total + 10;
            SharedPreferences.Editor editor = preferences.edit();
            editor.putInt("TotalPuntos",puntos);
            editor.commit();


            total = preferences.getInt("TotalPuntos",0);
            Log.d("puntos que lleva", String.valueOf(total));

        }
    }
}
