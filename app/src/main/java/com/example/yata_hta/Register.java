package com.example.yata_hta;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Register extends AppCompatActivity {

    private static final String URL = "https://yatahta.juanjodev.es/auth/register/";
    private static final String TOKENS = "Tokens";
    private Button accept;
    private EditText medicalCode;
    private EditText userCode;
    private EditText psw1;
    private EditText psw2;
    private EditText nickName;
    private EditText birhtday;
    private Context context;
    private AppCompatImageView initPiker;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        init();


    }


    private void init(){
        //SharePrefereces
        preferences = this.getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
        editor = preferences.edit();
        context = this;
        medicalCode = findViewById(R.id.codMedico);
        userCode = findViewById(R.id.conPatien);
        psw1 = findViewById(R.id.password1);
        psw2 = findViewById(R.id.password2);
        nickName = findViewById(R.id.UserNick);
        initPiker = findViewById(R.id.diaInicio);
        birhtday = findViewById(R.id.birthday);

        initPiker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar calendar = Calendar.getInstance();
                final int day = calendar.get(Calendar.DAY_OF_MONTH);
                final int month=  calendar.get(Calendar.MONTH);
                final int year = calendar.get(Calendar.YEAR);

                DatePickerDialog datePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int y, int m, int d) {
//                        birhtday.setText(d + "/" + (m+1) + "/" + y);
                        birhtday.setText(d + "/" + (m+1) + "/" + y);
                    }
                },year,month,day);

                datePickerDialog.show();
            }
        });

        accept = findViewById(R.id.accept_btn);
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if ( allIsCorrect()){
                    userCode();
//                    userRegister();
                }

            }
        });
    }


    private Boolean allIsCorrect(){
        String doctorId = medicalCode.getText().toString();
        String userId = userCode.getText().toString();
        String nick= nickName.getText().toString();
        String pwd1 =   psw1.getText().toString();
        String pwd2=   psw2.getText().toString();
        String birhdayUser = birhtday.getText().toString() ;


        medicalCode.setError(null);
        userCode.setError(null);
        nickName.setError(null);
        psw1.setError(null);
        psw2.setError(null);
        birhtday.setError(null);


        // Flag para evidenciar algun error durante la validación de los datos
        // Variable para contener el campo a ser enfocado
        boolean cancel = false;
        View focusView = null;

        if(TextUtils.isEmpty(doctorId)){
            medicalCode.setError("campo requerido");
            focusView = medicalCode;
            cancel = true;
        }

        if(TextUtils.isEmpty(userId)){
            userCode.setError("campo requerido");
            focusView = userCode;
            cancel = true;
        }

        if(TextUtils.isEmpty(nick)){
            nickName.setError("campo requerido");
            focusView = nickName;
            cancel = true;
        }


        if(!TextUtils.isEmpty(pwd2) && !pwd1.equals(pwd2)){
            psw2.setError("introduzca la misma contraseña en los dos campos");
            focusView = psw2;
            cancel = true;
        }else if(TextUtils.isEmpty(pwd2)){
            psw2.setError("campo requerido");
            focusView = psw2;
            cancel = true;
        }

        if(!TextUtils.isEmpty(pwd1) && !pwd1.equals(pwd2)){
            psw1.setError("introduzca la misma contraseña en los dos campos");
            focusView = psw1;
            cancel = true;
        }else if(TextUtils.isEmpty(pwd1)){
            psw1.setError("campo requerido");
            focusView = psw1;
            cancel = true;
        }

        if(TextUtils.isEmpty(birhdayUser)){
            birhtday.setError("campo requerido");
            focusView = birhtday;
            cancel = true;
        }


        if(cancel){
            // Enfocar el Campo del Error
            focusView.requestFocus();
            return false;
        }

        return true;
    }


    private void userRegister(){

        JSONObject param = new JSONObject();
        try {

            param.put("patient_code",userCode.getText().toString());
            param.put("doctor", medicalCode.getText().toString());
            param.put("username", nickName.getText().toString());


            String oldDateString = birhtday.getText().toString();
            String newDateString;

            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            try{
                Date d = sdf.parse(oldDateString);
                sdf.applyPattern("yyyy-MM-dd");
                newDateString = sdf.format(d);
                param.put("birthday",newDateString);
            }catch (Exception e){
                Log.d("PARSE", "Error en parsear fecha");
            }


//            param.put("birthday", birhtday.getText().toString() );
            param.put("password", psw1.getText().toString());
            param.put("password2",psw2.getText().toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL, param,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Toast.makeText(context,"Se ha registrado correctamente", Toast.LENGTH_LONG).show();
                        Intent a = new Intent(context, Login.class);

                        try {
                            editor.putString("doctorID",response.getString("doctor"));
                            editor.putString("userName", response.getString("username"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        editor.apply();
                        startActivity(a);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                       parseVolleyError(error);


//                        if(error instanceof ServerError){
//                            Log.d("TAG",error.toString());
//                            Toast.makeText(context,"No se ha podido registrar", Toast.LENGTH_LONG).show();
//                        }
//                        if(error instanceof NoConnectionError){//no conexion internet
//                            Log.d("TAG","No hay conexion a internet");
//                            Toast.makeText(context,"No se ha podido registrar", Toast.LENGTH_LONG).show();
//                        }
//
//                        if(error instanceof NetworkError){//desconexionsocket
//                            Log.d("TAG","NetWorkError");
//                            Toast.makeText(context,"No se ha podido registrar", Toast.LENGTH_LONG).show();
//                        }

                    }
                }){


        };

        VolleySingleton.getInstaceVolley(getApplicationContext()).addToRequestQueue(request);
    }

    private void userCode(){
        String URL_USER_CODE = "https://yatahta.juanjodev.es/api/patient_codes/";

        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET,URL_USER_CODE , null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        boolean encontrado = false;
                        int i = 0;
                        while (i<response.length() && !encontrado){
                            try {
                                JSONObject user = response.getJSONObject(i);
                                if ( user.getString("code").equals(userCode.getText().toString())){
                                    encontrado = true;
                                }
                                i++;

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        if (encontrado == false){

                            userCode.setError(null);

                            boolean cancel = false;
                            View focusView;

                            userCode.setError("Codigo Paciente no válido");
                            focusView = userCode;
                            focusView.requestFocus();

                        }else {
                            userRegister();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        parseVolleyError(error);

//                        if(error instanceof ServerError){
//                            Log.d("TAG",error.toString());
//                            Toast.makeText(context,"No se ha podido registrar", Toast.LENGTH_LONG).show();
//                        }
//                        if(error instanceof NoConnectionError){//no conexion internet
//                            Log.d("TAG","No hay conexion a internet");
//                            Toast.makeText(context,"No se ha podido registrar", Toast.LENGTH_LONG).show();
//                        }
//
//                        if(error instanceof NetworkError){//desconexionsocket
//                            Log.d("TAG","NetWorkError");
//                            Toast.makeText(context,"No se ha podido registrar", Toast.LENGTH_LONG).show();
//                        }

                    }
                }){


                    };

        VolleySingleton.getInstaceVolley(getApplicationContext()).addToRequestQueue(request);
    }


    private void parseVolleyError(VolleyError error) {
        try {
            String responseBody = new String(error.networkResponse.data, "utf-8");
            JSONObject data = new JSONObject(responseBody);


//            JSONArray doctorErrors = data.getJSONArray("doctor");
//            JSONArray PasswordErrors = data.getJSONArray("password");
            String doctorResponse = "";
            String passwordRespone= "";
            String nickNameResponse = "";
            String medicalResponse = "";

            medicalCode.setError(null);
            userCode.setError(null);
            nickName.setError(null);
            psw1.setError(null);
            psw2.setError(null);
            birhtday.setError(null);


            boolean cancel = false;
            View focusView = null;

            try {
                if(!data.getJSONArray("doctor").isNull(0)){
                    JSONArray doctorErrors = data.getJSONArray("doctor");
                    for ( int i = 0; i< doctorErrors.length(); i++){
                        doctorResponse += doctorErrors.get(i)+ "\n";
                    }
                    medicalCode.setError(doctorResponse);
                    focusView = medicalCode;
                    cancel = true;
                }

            }catch (Exception e){

            }

            try {
                if(!data.getJSONArray("password").isNull(0)){
                    JSONArray PasswordErrors = data.getJSONArray("password");
                    for ( int i = 0; i< PasswordErrors.length(); i++){
                        passwordRespone += PasswordErrors.get(i)+ "\n";
                    }
                    psw1.setError(passwordRespone);
                    focusView = psw1;
                    cancel = true;
                }

            }catch (Exception e){

            }

            try {
                if(!data.getJSONArray("username").isNull(0)){
                    JSONArray usernameErrors = data.getJSONArray("username");
                    for ( int i = 0; i< usernameErrors.length(); i++){
                        nickNameResponse += usernameErrors.get(i)+ "\n";
                    }
                    nickName.setError(nickNameResponse);
                    focusView = nickName;
                    cancel = true;
                }

            }catch (Exception e){

            }

//            try {
//                if(!data.getJSONArray("doctor").isNull(0)){
//                    JSONArray doctorIdErrors = data.getJSONArray("doctor");
//                    for ( int i = 0; i< doctorIdErrors.length(); i++){
//                        medicalResponse += doctorIdErrors.get(i)+ "\n";
//                    }
//                    medicalCode.setError(medicalResponse);
//                    focusView = medicalCode;
//                    cancel = true;
//                }
//
//            }catch (Exception e){
//
//            }






//            if (!doctorErrors.isNull(0)){
//                for ( int i = 0; i< doctorErrors.length(); i++){
//                    doctorResponse += doctorErrors.get(i)+ "\n";
//                }
//                medicalCode.setError(doctorResponse);
//                focusView = medicalCode;
//                cancel = true;
//
//            }

//            if(!PasswordErrors.isNull(0)){
//                for ( int i = 0; i< PasswordErrors.length(); i++){
//                    passwordRespone += PasswordErrors.get(i)+ "\n";
//                }
//                psw1.setError(passwordRespone);
//                focusView = psw1;
//                cancel = true;
//            }



            if(cancel){
                // Enfocar el Campo del Error
                focusView.requestFocus();
            }


        } catch (JSONException | UnsupportedEncodingException e) {
        }
    }
}