package com.example.yata_hta;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;

import androidx.appcompat.app.AppCompatActivity;

public class About extends AppCompatActivity {

     Button about;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_hta);
        about = (Button) findViewById(R.id.ok1);

    }


    public void entendido (View view){
        Intent intent = new Intent(this, User.class);
        startActivity(intent);
    }
}
