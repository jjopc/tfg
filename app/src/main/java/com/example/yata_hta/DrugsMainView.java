package com.example.yata_hta;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.yata_hta.recyclerViewAdapters.DrugRecyclerViewAdapter;
import com.example.yata_hta.temporary.Drug;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DrugsMainView extends AppCompatActivity  implements DrugRecyclerViewAdapter.OnNoteListener{
    public static final int ADD_DRUG_REQUEST = 1;
    private static final String TAG = DrugsMainView.class.getSimpleName();
    public List<Drug> drugs;
    private Context context;
    private RecyclerView drugsRecyclerView;
    private DrugRecyclerViewAdapter drugRecyclerViewAdapter;
    private FloatingActionButton fab;
    public List<String> enlaces;
    BottomNavigationView navigation;
    BottomNavigationView drugNavigation;
    String URL = "https://yatahta.juanjodev.es/api/users/";

    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    String userID;
    String token = "";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drugs_main_view);

        init();
    }
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(context, User.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onResume () {
        super.onResume();
        getDrugs();
    }

    private void init(){
        //SharePrefereces
        preferences= this.getSharedPreferences("LOGIN",this.MODE_PRIVATE);
        editor = preferences.edit();
        token = preferences.getString("access" ,"");
        userID = String.valueOf(preferences.getInt("userID",0));
        URL += userID + "/posologies/" ;

        context = this;
        drugsRecyclerView = findViewById(R.id.drugs_recycler_view);
        fab = findViewById(R.id.drugs_add_new_fab);
        drugs = new ArrayList<Drug>();
        enlaces = new ArrayList<>();
        drugRecyclerViewAdapter = new DrugRecyclerViewAdapter(this,context, token);
        // RequestQueue queue = VolleySingleton.getInstaceVolley(this.getApplicationContext()).getRequestQueue();

        drugsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        drugsRecyclerView.setAdapter(drugRecyclerViewAdapter);

        navigation = findViewById(R.id.bottom_navigation);
        navigation.setSelectedItemId(R.id.medication);
        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.pressure:
                        startActivity(new Intent(getApplicationContext(), Pressures.class));
                        overridePendingTransition(0, 0);
                        return true;
                    case R.id.medication:
                        return true;
                    case R.id.user:
                        startActivity(new Intent(getApplicationContext(), User.class));
                        overridePendingTransition(0, 0);
                        return true;
                    case R.id.mail:
                        startActivity(new Intent(getApplicationContext(), Chat.class));
                        overridePendingTransition(0, 0);
                        return true;

                }
                return false;
            }
        });

        drugNavigation = findViewById(R.id.medication_mode);
        drugNavigation.setSelectedItemId(R.id.medication);
        drugNavigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.medication_points:
                        Intent intent = new Intent(getApplicationContext(), DrugListPoints.class);
                        intent.putExtra("listaMedicacion", (Serializable) drugs);
                        startActivity(intent);
                        overridePendingTransition(0, 0);
                        return true;
                    case R.id.medication2:
                        return true;


                }
                return false;
            }
        });
    }


    public void addDrug(View view) {
        Log.d(TAG, "Parece que funciona el FAB");
        Intent intent = new Intent(this, AddNewDrugActivity.class);
        startActivity(intent);
    }

    /*
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ADD_DRUG_REQUEST) {
            if (resultCode == RESULT_OK) {
                Log.d(TAG, "Parece que funciona el replyIntent");
                drugs = new ArrayList<>();
                drugs = (List<Drug>) data.getSerializableExtra("drugList");
                drugRecyclerViewAdapter.setDrugEntityList(drugs);
            } else if (resultCode == RESULT_CANCELED) {
                Log.d(TAG, "Operación cancelada");
            }
        }

    }*/


    private void getDrugs(){
        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, URL, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                      //  if (drugs.size() == 0) {
                            try {

                                int size = response.length();
                                for (int i = 0; i < size; i++) {
                                    JSONObject medicina = response.getJSONObject(i);
                                   // JSONObject drugObject = medicina.getJSONObject("drug");
                                    Drug drug = new Drug(
                                            medicina.getString("id"),
                                            medicina.getString("drug_name"),
                                            medicina.getBoolean("at_breakfast_taken"),
                                            medicina.getString("at_breakfast"),
                                            medicina.getBoolean("at_lunch_taken"),
                                            medicina.getString("at_lunch"),
                                            medicina.getBoolean("at_dinner_taken"),
                                            medicina.getString("at_dinner"),
                                            medicina.getBoolean("at_bedtime_taken"),
                                            medicina.getString("at_bedtime"),
                                            medicina.getString("start_date"),
                                            medicina.getString("end_date"),
                                            medicina.getString("user"),
                                            medicina.getString("drug")

                                    );
                                    drugs.add(drug);

                                }


                                drugRecyclerViewAdapter.setDrugEntityList(drugs);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                     //   }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(error instanceof ServerError){
                            Log.d("TAG","Error en servidor");
                        }
                        if(error instanceof NoConnectionError){//no conexion internet
                            Log.d("TAG","No hay conexion a internet");
                        }

                        if(error instanceof NetworkError){//desconexionsocket
                            Log.d("TAG","NetWorkError");
                        }

                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                //String credentials = "Dinna:12345678";
                //String auth = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                //headers.put("Authorization", auth);
                headers.put("Authorization", "Bearer" + " " + token);//App.getToken()

                return headers;
            }
                };

        VolleySingleton.getInstaceVolley(getApplicationContext()).addToRequestQueue(request);


    }

    @Override
    public void onNoteClick(int position) {
        Intent intent = new Intent(context,InfoPosologie.class);
        Drug drug = drugs.get(position);
        Gson gson = new Gson();
        String drugGson = gson.toJson(drug);
        intent.putExtra("infoDrug", drugGson);
        startActivity(intent);
    }



/*
    private void getdrugInfo (String url){

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            try {
                                String name = response.getString("name");
                                Drug drug = new Drug(name);
                                drugs.add(drug);
                                drugRecyclerViewAdapter.setDrugEntityList(drugs);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if(error instanceof ServerError){
                        Log.d("TAG","Error en servidor");
                    }
                    if(error instanceof NoConnectionError){//no conexion internet
                        Log.d("TAG","No hay conexion a internet");
                    }

                    if(error instanceof NetworkError){//desconexionsocket
                        Log.d("TAG","NetWorkError");
                    }
                }
            });


        VolleySingleton.getInstaceVolley(getApplicationContext()).addToRequestQueue(request);
    }*/

}
