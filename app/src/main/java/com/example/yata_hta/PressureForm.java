package com.example.yata_hta;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;


import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class PressureForm extends AppCompatActivity {

    EditText sys;
    EditText dias;
    EditText heard;
    Spinner arm;
    EditText comm;
    String sistolic;
    String diastolic;
    String bpm;
    String h;
    String a;
    String c;
    String URL = "https://yatahta.juanjodev.es/api/pressures/";
   // String URL = "https://yatahta.juanjodev.es/api/users/";
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    String userID;
    String token = "";
    private Context context;

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(context, Pressures.class);
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tension_form);

        //SharePrefereces
        preferences= this.getSharedPreferences("LOGIN",this.MODE_PRIVATE);
        editor = preferences.edit();
        userID = String.valueOf(preferences.getInt("userID",0));
        token = preferences.getString("access" ,"");
        //URL += userID + "/pressures/" ;

        sys = findViewById(R.id.editTextNumberDecimal);
        dias = findViewById(R.id.editTextNumberDecimal2);
        heard= findViewById(R.id.editTextNumberDecimal3);
        arm = findViewById(R.id.armSelector);

        context = this;
        Button save = findViewById(R.id.guardar_tension);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (allIsCorrect()){

                    AlertDialog.Builder alert = new AlertDialog.Builder(context);
                    alert.setMessage("¿Desea agregar la toma de tensión?")
                            .setCancelable(false)
                            .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    pushPreassure();
                                    Intent a = new Intent(context, Pressures.class);
                                    startActivity(a);
                                }
                            })
                            .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });

                    AlertDialog titulo = alert.create();
                    titulo.setTitle("Añadir tensión");
                    titulo.show();


                }

//                if (Integer.parseInt(s) > 170 || Integer.parseInt(s)< 90  || Integer.parseInt(d) > 110 || Integer.parseInt(d)< 50 ){
//                    Toast.makeText(context, "valores de la toma fuera de rango",Toast.LENGTH_LONG).show();
//                }else{
//
//                    pushPreassure();
//                    Intent a = new Intent(context, Pressures.class);
//                    startActivity(a);
//                }

            }
        });



    }


    private Boolean allIsCorrect(){
        sistolic = sys.getText().toString();
        diastolic = dias.getText().toString();
        bpm = heard.getText().toString();

        sys.setError(null);
        dias.setError(null);
        heard.setError(null);

        // Flag para evidenciar algun error durante la validación de los datos
        // Variable para contener el campo a ser enfocado
        boolean cancel = false;
        View focusView = null;



        if(!TextUtils.isEmpty(sistolic) && (Integer.parseInt(sistolic) > 240 || Integer.parseInt(sistolic)< 60)){
            sys.setError("Introducir valores entre 60-240");
            focusView = sys;
            cancel = true;
        }else if(TextUtils.isEmpty(sistolic)){
            sys.setError("campo requerido");
            focusView = sys;
            cancel = true;
        }

        if(!TextUtils.isEmpty(diastolic) && (Integer.parseInt(diastolic) > 140 || Integer.parseInt(diastolic)< 30)){
            dias.setError("Introducir valores entre 30-140");
            focusView = dias;
            cancel = true;
        }else if(TextUtils.isEmpty(diastolic)){
            dias.setError("campo requerido");
            focusView = dias;
            cancel = true;
        }

        if(!TextUtils.isEmpty(bpm) && (Integer.parseInt(bpm) > 250 || Integer.parseInt(bpm)< 40)){
            heard.setError("Introducir valores entre 40-250");
            focusView = heard;
            cancel = true;
        }else if(TextUtils.isEmpty(bpm)){
            heard.setError("campo requerido");
            focusView = heard;
            cancel = true;
        }




        if(cancel){
            // Enfocar el Campo del Error
            focusView.requestFocus();
            return false;
        }

            return true;
    }




    private void pushPreassure() {

        sistolic = sys.getText().toString();
        diastolic = dias.getText().toString();
        h = heard.getText().toString();
        a = arm.getSelectedItem().toString();






        if (a.equals("Izquierdo")){
            a = "LT";
        }else{
            a = "RT";
        }
        JSONObject param = new JSONObject();
        try {

            param.put("systolic", sistolic);
            param.put("diastolic", diastolic);
            param.put("arm", a);
            param.put("bpm", h);
            param.put("user", userID);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL, param,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Toast.makeText(context, "Tensión añadida",Toast.LENGTH_LONG).show();
                        Log.d("TAG","Success");

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(error instanceof ServerError){
                            Log.d("TAG","Error en servidor");
                        }
                        if(error instanceof NoConnectionError){//no conexion internet
                            Log.d("TAG","No hay conexion a internet");
                        }

                        if(error instanceof NetworkError){//desconexionsocket
                            Log.d("TAG","NetWorkError");
                        }

                    }
                }){
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> headers = new HashMap<>();
                        // String credentials = "Dinna:12345678";
                        // String auth = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                        // headers.put("Authorization", auth);
                        headers.put("Authorization", "Bearer" + " " + token);//App.getToken()
                        return headers;
                    }
        };

        VolleySingleton.getInstaceVolley(getApplicationContext()).addToRequestQueue(request);
    }
}
