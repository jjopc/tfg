package com.example.yata_hta;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.AlarmClock;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class AlarmSettings extends AppCompatActivity {
    private AppCompatImageView BreakfastPiker;
    private AppCompatImageView LunchPicker;
    private AppCompatImageView DinnerPiker;
    private AppCompatImageView BedPicker;

    private AppCompatImageView BreakAlarm;
    private AppCompatImageView LunchAlarm;
    private AppCompatImageView DinnerAlarm;
    private AppCompatImageView BedAlarm;

    private TextView DateBreakfast;
    private TextView DateLunch;
    private TextView DateDinner;
    private TextView DateBed;
    int hourBreakfas, hourLunch,hourDinner,hourBed;
    int minuBreakfast, minuLunch, minuDinner,minuBed;

    private Button btnAcept;
    private Button btnBack;
    private Context context;

    private SharedPreferences preferences;
    SharedPreferences.Editor editor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm_settings);
        init();
    }
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(context, User.class);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();

        DateBreakfast.setText(preferences.getString("breakfast",""));
        DateLunch.setText(preferences.getString("lunch",""));
        DateDinner.setText(preferences.getString("dinner",""));
        DateBed.setText(preferences.getString("bed",""));
    }


    private void init(){
        BreakfastPiker = findViewById(R.id.calendarioDes);
        LunchPicker = findViewById(R.id.diaIniciocomida);
        DinnerPiker = findViewById(R.id.diaIniciocena);
        BedPicker =  findViewById(R.id.diaIniciocama);

        BreakAlarm = findViewById(R.id.setAlarmDes);
        LunchAlarm= findViewById(R.id.setAlarmCom);
        DinnerAlarm= findViewById(R.id.setAlarmCen);
        BedAlarm= findViewById(R.id.setAlarmBed);

        DateBreakfast = findViewById(R.id.initDateDes);
        DateLunch = findViewById(R.id.initDatecomi);
        DateDinner = findViewById(R.id.initDatecena);
        DateBed = findViewById(R.id.initDatecama);


        btnAcept = findViewById(R.id.accept_btn);
        btnBack = findViewById(R.id.back_btn);
        context = this;

        preferences= context.getSharedPreferences("ALARM",this.MODE_PRIVATE);
        editor = preferences.edit();






        BreakfastPiker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                TimePickerDialog timePickerDialog = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        hourBreakfas = hourOfDay;
                        minuBreakfast = minute;
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(0,0,0,hourBreakfas,minuBreakfast);
                        DateBreakfast.setText(DateFormat.format("HH:mm ",calendar));
                        editor.putString("breakfast",DateBreakfast.getText().toString());
                        editor.apply();
                    }
                },12,0,true);


                timePickerDialog.updateTime(hourBreakfas,minuBreakfast);
                timePickerDialog.show();

            }
        });
        BreakAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!DateBreakfast.getText().toString().isEmpty()){
                    //BreakAlarm.setImageResource(R.drawable.ic_setalarm);
                    BreakAlarm.setColorFilter(Color.BLACK);
                    Intent a = createAlarm(DateBreakfast);
                    //a.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(a);
                }else{
                    Toast.makeText(context, "Elija una hora para el Desayuno",Toast.LENGTH_LONG).show();
                }
            }
        });


        LunchPicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimePickerDialog timePickerDialog = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        hourLunch = hourOfDay;
                        minuLunch = minute;
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(0,0,0,hourLunch,minuLunch);
                        DateLunch.setText(DateFormat.format("HH:mm ",calendar));
                        editor.putString("lunch",DateLunch.getText().toString());
                        editor.apply();
                    }
                },12,0,true);

                timePickerDialog.updateTime(hourLunch,minuLunch);
                timePickerDialog.show();

            }
        });
        LunchAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!DateLunch.getText().toString().isEmpty()){
                    LunchAlarm.setColorFilter(Color.BLACK);
                    Intent a = createAlarm(DateLunch);
                    //a.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(a);
                }else{
                    Toast.makeText(context, "Elija una hora para la comida",Toast.LENGTH_LONG).show();
                }
            }
        });

        DinnerPiker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                TimePickerDialog timePickerDialog = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        hourDinner= hourOfDay;
                        minuDinner = minute;
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(0,0,0,hourDinner,minuDinner);
                        DateDinner.setText(DateFormat.format("HH:mm aa",calendar));
                        editor.putString("dinner",DateDinner.getText().toString());
                        editor.apply();
                    }
                },12,0,true);

                timePickerDialog.updateTime(hourDinner,minuDinner);
                timePickerDialog.show();

            }
        });
        DinnerAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!DateDinner.getText().toString().isEmpty()){
                    DinnerAlarm.setColorFilter(Color.BLACK);
                    Intent a = createAlarm(DateDinner);
                    //a.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(a);
                }else{
                    Toast.makeText(context, "Elija una hora para la cena",Toast.LENGTH_LONG).show();
                }
            }
        });

        BedPicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                TimePickerDialog timePickerDialog = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        hourBed= hourOfDay;
                        minuBed = minute;
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(0,0,0,hourBed,minuBed);
                        DateBed.setText(DateFormat.format("HH:mm:aa",calendar));
                        editor.putString("bed",DateBed.getText().toString());
                        editor.apply();
                    }
                },12,0,true);

                timePickerDialog.updateTime(hourBed,minuBed);
                timePickerDialog.show();

            }
        });
        BedAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!DateBed.getText().toString().isEmpty()){
                    BedAlarm.setColorFilter(Color.BLACK);
                    Intent a = createAlarm(DateBed);
                    //a.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(a);
                }else{
                    Toast.makeText(context, "Elija una hora para la cama",Toast.LENGTH_LONG).show();
                }
            }
        });



        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context,  User.class);
                startActivity(intent);
            }
        });





    }

    private Intent createAlarm(TextView hora){
        String hour= hora.getText().toString();

        int h = Character.getNumericValue(hour.charAt(0)) *10 + Character.getNumericValue(hour.charAt(1));
        int m =Character.getNumericValue(hour.charAt(3)) *10 + Character.getNumericValue(hour.charAt(4));
        ArrayList<Integer> days = new ArrayList<Integer>();
        days.add(Calendar.MONDAY);
        days.add(Calendar.TUESDAY);
        days.add(Calendar.WEDNESDAY);
        days.add(Calendar.THURSDAY);
        days.add(Calendar.FRIDAY);
        days.add(Calendar.SATURDAY);
        days.add(Calendar.SUNDAY);


        Intent intent = new Intent(AlarmClock.ACTION_SET_ALARM);
        intent.putExtra(AlarmClock.EXTRA_HOUR, h);
        intent.putExtra(AlarmClock.EXTRA_MINUTES,m);
        intent.putExtra(AlarmClock.EXTRA_MESSAGE,"Tómese la medicación");
        intent.putExtra(AlarmClock.EXTRA_DAYS,days);
        intent.putExtra(AlarmClock.EXTRA_SKIP_UI,true);

        return intent;
    }
}