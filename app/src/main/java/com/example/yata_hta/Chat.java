package com.example.yata_hta;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.yata_hta.databinding.ActivityChatBinding;
import com.example.yata_hta.recyclerViewAdapters.ChatRecyclerViewAdapter;
import com.example.yata_hta.temporary.ChatMessage;
import com.example.yata_hta.temporary.Drug;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Time;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Chat extends AppCompatActivity {

    private ActivityChatBinding binding;
    private User receiverUser;//este user no es el real

    private List<ChatMessage> chatList;
    private ChatRecyclerViewAdapter adapter;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    String doctorID;
    String userID;
    String token = "";

    String URL = "https://yatahta.juanjodev.es/api/users/";
    String URL2 = "https://yatahta.juanjodev.es/api/messages/";
    String URLCHAT = "https://yatahta.juanjodev.es/api/chat/";

    private Context context;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityChatBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
       init();

    }
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(context, User.class);
        startActivity(intent);
        finish();
    }


    @Override
    protected void onResume () {
        super.onResume();
        getChat();
    }

    private void init(){

        //SharePrefereces
        preferences= this.getSharedPreferences("LOGIN",this.MODE_PRIVATE);
        editor = preferences.edit();


        token = preferences.getString("access" ,"");
        userID = String.valueOf(preferences.getInt("userID",0));
        doctorID = preferences.getString("doctorID","");
        URL += userID + "/messages/" ;
        URLCHAT += userID+ "/";
        context = this;


        chatList = new ArrayList<>();
        //  binding.chatRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new ChatRecyclerViewAdapter(userID);
        binding.chatRecyclerView.setAdapter(adapter);
        binding.layoutSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pushChat();

            }
        });

        binding.refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent refresh = new Intent(context, Chat.class);
                startActivity(refresh);
                finish();
            }
        });
        binding.bottomNavigation.setSelectedItemId(R.id.mail);
        binding.bottomNavigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.pressure:
                        startActivity(new Intent(getApplicationContext(), Pressures.class));
                        overridePendingTransition(0, 0);
                        return true;
                    case R.id.medication:
                        startActivity(new Intent(getApplicationContext(), DrugsMainView.class));
                        overridePendingTransition(0, 0);
                        return true;
                    case R.id.user:
                        startActivity(new Intent(getApplicationContext(), User.class));
                        overridePendingTransition(0, 0);
                        return true;
                    case R.id.mail:
                        return true;

                }
                return false;
            }
        });

    }

    private void pushChat(){

        JSONObject param = new JSONObject();
        try {
            String chat = binding.inputMessage.getText().toString();
            param.put("text",chat);
            param.put("read",false);
            param.put("transmitter",userID);
            param.put("receiver", doctorID);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL2, param,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Toast.makeText(context, "Mensaje enviado",Toast.LENGTH_LONG).show();
                        Log.d("TAG","Success");
                        binding.inputMessage.setText("");
                        chatList.clear();
                        getChat();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(error instanceof ServerError){
                            Log.d("TAG","Error en servidor");
                        }
                        if(error instanceof NoConnectionError){//no conexion internet
                            Log.d("TAG","No hay conexion a internet");
                        }

                        if(error instanceof NetworkError){//desconexionsocket
                            Log.d("TAG","NetWorkError");
                        }

                    }
                }){

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> headers = new HashMap<>();
                        //String credentials = "Dinna:12345678";
                        //String auth = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                        //headers.put("Authorization", auth);
                        headers.put("Authorization", "Bearer" + " " + token);//App.getToken()


                        return headers;
                    }
                    };

        VolleySingleton.getInstaceVolley(getApplicationContext()).addToRequestQueue(request);
    }

    private void getChat() {

         JsonArrayRequest request= new JsonArrayRequest(Request.Method.GET, URL, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                       try {

                           for (int i = 0 ; i < response.length(); i++){
                               JSONObject mensaje = response.getJSONObject(i);

                               String transmiter = mensaje.getString("transmitter".toString());
                               String receiver = mensaje.getString("receiver".toString());
                               String dataTime = mensaje.getString("date");

                               int j= 0;
                               String fecha = "";
                               boolean encontrado= false;
                               while(j < dataTime.length() && !encontrado){
                                   if(dataTime.charAt(j) == 'T'){
                                       encontrado=true;
                                   }else{
                                       fecha += dataTime.charAt(j);
                                       j++;
                                   }

                               }

                               String text = mensaje.getString("text");
                               ChatMessage chat = new ChatMessage(transmiter,receiver,text,fecha);
                               chatList.add(chat);
                           }

                            Collections.sort(chatList, new Comparator<ChatMessage>() {
                                @Override
                                public int compare(ChatMessage o1, ChatMessage o2) {

                                    if(o1.getDataTime().compareTo(o2.getDataTime()) < 0){
                                        return -1;
                                    }else if (o1.getDataTime().compareTo(o2.getDataTime()) > 0){
                                        return 1;
                                    }
                                    return 0;
                                }
                            });

                            adapter.setChat(chatList);
                            binding.chatRecyclerView.scrollToPosition(chatList.size()-1);
                            binding.chatRecyclerView.setVisibility(View.VISIBLE);

                        }catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if(error instanceof ServerError){
                                Log.d("TAG","Error en servidor");
                            }
                            if(error instanceof NoConnectionError){//no conexion internet
                                Log.d("TAG","No hay conexion a internet");
                            }

                            if(error instanceof NetworkError){//desconexionsocket
                                Log.d("TAG","NetWorkError");
                            }

                        }
                     }){
                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            Map<String, String> headers = new HashMap<>();
                           // String credentials = "Dinna:12345678";
                           // String auth = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                           // headers.put("Authorization", auth);
                            headers.put("Authorization", "Bearer" + " " + token);//App.getToken()


                            return headers;
                        }
                         };


        VolleySingleton.getInstaceVolley(getApplicationContext()).addToRequestQueue(request);
    }




/*
    private void init(){
        mensage  = new ArrayList<>();

        ChatMessage m = new ChatMessage("DoctorMateo","hola como estas", "25/02/2022");
        mensage.add(m);
        mensage.add(new ChatMessage("DoctorMateo","mensaje de prueba","25/05/2022"));

        adapter = new ChatRecyclerViewAdapter(mensage,null,"DoctorMateo");
        binding.chatRecyclerView.setAdapter(adapter);

        binding.layoutSend.setOnClickListener( v -> senMensage());
    }

    private void senMensage() {
        ChatMessage a = new ChatMessage("Yo", binding.inputMessage.getText().toString(), "24/02/2022");
        mensage.add(a);
       // binding.inputMessage.setText(null);
        adapter.setChat(mensage);
    }
*/
}