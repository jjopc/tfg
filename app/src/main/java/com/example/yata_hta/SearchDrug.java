package com.example.yata_hta;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.yata_hta.recyclerViewAdapters.SearchDrugsAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SearchDrug extends AppCompatActivity {


    private RecyclerView recyclerView;
    public static String drugName;
    private String searchDrug;
    private static List<String> listDrugs;
    private SearchDrugsAdapter searchDrugsAdapter;
    private Button acept;
    private Context context;
    String URL = "https://cima.aemps.es/cima/rest/medicamentos?nombre=";

    private SharedPreferences preferences;
    SharedPreferences.Editor editor;
    String token = "";





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_drug);
        init();
    }











    private void init(){

        preferences= this.getSharedPreferences("LOGIN",this.MODE_PRIVATE);
        editor = preferences.edit();
        token = preferences.getString("access" ,"");


        context = this;
        listDrugs = new ArrayList<>();
        recyclerView = findViewById(R.id.recyclerView);
        drugName = "";
        searchDrugsAdapter = new SearchDrugsAdapter(context);
        recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        recyclerView.setAdapter(searchDrugsAdapter);

        acept = findViewById(R.id.nameDrudGutton);

        //Obtengo Intent
        Intent intent = getIntent();
        searchDrug = intent.getStringExtra("searchDrug");
        URL+=searchDrug;

        getDrugs();


        // Listener
        acept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentRespuesta = new Intent();
                intentRespuesta.putExtra("searchDrug", drugName);
                setResult(RESULT_OK,intentRespuesta);
                finish();

            }
        });



    }


    private void getDrugs() {
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, URL, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                           JSONArray result = response.getJSONArray("resultados");
                            int size = result.length();


                            for (int i = 0; i < size; i++){
                                JSONObject registro = result.getJSONObject(i);
                                listDrugs.add(registro.getString("nombre"));
                            }
                            searchDrugsAdapter.setListDrus(listDrugs);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof ServerError) {
                            Log.d("TAG", "Error en servidor");
                        }
                        if (error instanceof NoConnectionError) {//no conexion internet
                            Log.d("TAG", "No hay conexion a internet");
                        }

                        if (error instanceof NetworkError) {//desconexionsocket
                            Log.d("TAG", "NetWorkError");
                        }

                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                //   String credentials = "Dinna:12345678";
                //     String auth = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                //    headers.put("Authorization", auth);

                headers.put("Authorization", "Bearer" + " " + token);//App.getToken()

                return headers;
            }
        };

        VolleySingleton.getInstaceVolley(getApplicationContext()).addToRequestQueue(request);


    }

}