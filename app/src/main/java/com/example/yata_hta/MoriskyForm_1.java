package com.example.yata_hta;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MoriskyForm_1 extends AppCompatActivity {

    private RadioGroup radioGroup1;
    private RadioGroup radioGroup2;
    private RadioGroup radioGroup3;
    private RadioGroup radioGroup4;

    private RadioButton selectedRadioButton1;
    private RadioButton selectedRadioButton2;
    private RadioButton selectedRadioButton3;
    private RadioButton selectedRadioButton4;

    private Button confirmButton;
    private String userID;
    private String token = "";

    private SharedPreferences preferences;
    private Context context;

    private String URL = "https://yatahta.juanjodev.es/api/moriskys/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_morisky_form1);
        init();
    }

    private void init() {
        preferences= this.getSharedPreferences("LOGIN",this.MODE_PRIVATE);
        userID = String.valueOf(preferences.getInt("userID",0));
        token = preferences.getString("access" ,"");
        context = this;

        radioGroup1 = findViewById(R.id.rg1);
        radioGroup2 = findViewById(R.id.rg2);
        radioGroup3 = findViewById(R.id.rg3);
        radioGroup4 = findViewById(R.id.rg4);
        confirmButton = findViewById(R.id.end);

        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int selectedRadioButtonId1 = radioGroup1.getCheckedRadioButtonId();
                int selectedRadioButtonId2 = radioGroup2.getCheckedRadioButtonId();
                int selectedRadioButtonId3 = radioGroup3.getCheckedRadioButtonId();
                int selectedRadioButtonId4 = radioGroup4.getCheckedRadioButtonId();

                if (    selectedRadioButtonId1 != -1 &&
                        selectedRadioButtonId2 != -1 &&
                        selectedRadioButtonId3 != -1 &&
                        selectedRadioButtonId4 != -1
                ) {

                    selectedRadioButton1 = findViewById(selectedRadioButtonId1);
                    selectedRadioButton2 = findViewById(selectedRadioButtonId2);
                    selectedRadioButton3 = findViewById(selectedRadioButtonId3);
                    selectedRadioButton4 = findViewById(selectedRadioButtonId4);
//                    String selectedRbText = selectedRadioButtonId1.getText().toString();
//                    textView.setText(selectedRbText + " is Selected");

                    pushMoriskyform(selectedRadioButton1,selectedRadioButton2,selectedRadioButton3,selectedRadioButton4);
                } else {
                    Toast.makeText(context, "Seleccione todos las preguntas",Toast.LENGTH_LONG).show();
                }
            }
        });




    }
    private void pushMoriskyform(RadioButton selec1,RadioButton selec2,RadioButton selec3,RadioButton selec4 ) {
        JSONObject param = new JSONObject();
        try {
            int cont = 0;

            if (selec1.getText().toString().equals("SI")){
                param.put("answer1",true);
            }else{
                param.put("answer1",false);
                cont++;
            }

            if (selec2.getText().toString().equals("SI")){
                param.put("answer2",true);
                cont++;
            }else{
                param.put("answer2",false);
            }


            if (selec3.getText().toString().equals("SI")){
                param.put("answer3",true);
            }else{
                param.put("answer3",false);
                cont++;
            }

            if (selec4.getText().toString().equals("SI")){
                param.put("answer4",true);
            }else{
                param.put("answer4",false);
                cont++;
            }

            if (cont == 4){
                param.put("isOk", true);
            }else{
                param.put("isOk", false);
            }

            param.put("user", userID);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL, param,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                            Intent intent = new Intent(context, BattleForm_1.class);
                            startActivity(intent);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof ServerError) {
                            Log.d("TAG", "Error en servidor");
                        }
                        if (error instanceof NoConnectionError) {//no conexion internet
                            Log.d("TAG", "No hay conexion a internet");
                        }

                        if (error instanceof NetworkError) {//desconexionsocket
                            Log.d("TAG", "NetWorkError");
                        }

                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                //   String credentials = "Dinna:12345678";
                //     String auth = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                //    headers.put("Authorization", auth);

                headers.put("Authorization", "Bearer" + " " + token);//App.getToken()

                return headers;
            }
        };

        VolleySingleton.getInstaceVolley(getApplicationContext()).addToRequestQueue(request);


    }

}