package com.example.yata_hta;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.yata_hta.temporary.Drug;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class InfoPosologie extends AppCompatActivity {

    private  String URL = "https://yatahta.juanjodev.es/api/posologies/";
    private Drug drug;

    private EditText drugName;
    private  EditText drugDosage;
    private CheckBox breakfast;
    private CheckBox lunch;
    private CheckBox dinner;
    private CheckBox bed;
    private EditText breakfastDoss;
    private EditText lunchDoss;
    private EditText dinnerDoss;
    private EditText bedDoss;
    private TextView initDate;
    private TextView endDate;
    private AppCompatImageView initPiker;
    private AppCompatImageView endPicker;
    private AppCompatImageView search;
    private AppCompatImageView moreOptions;
    private EditText dossageFrec;
    private EditText freq;
    private Spinner dosageSpinner;
    private Spinner frequencySpinner;
    private Button btnAccept;
    private LinearLayout linearLayout;
    private Context context;

    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    String token = "";
    String userID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_posologie);
        init();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(context, DrugsMainView.class);
        startActivity(intent);
    }

    private void init() {
        Gson gson = new Gson();
        drug = gson.fromJson(getIntent().getStringExtra("infoDrug"),Drug.class);

        context= this;
        //SharePrefereces
        preferences= this.getSharedPreferences("LOGIN",this.MODE_PRIVATE);
        token = preferences.getString("access" ,"");
        userID = String.valueOf(preferences.getInt("userID",0));


        drugName = findViewById(R.id.drugNameEditText);
        drugDosage = findViewById(R.id.drugDosageEditText);
        breakfast = findViewById(R.id.desayunoCheckBox);
        lunch = findViewById(R.id.comidaCheckBox);
        dinner = findViewById(R.id.cenaCheckBox);
        bed = findViewById(R.id.dormirCheckBox);
        breakfastDoss = findViewById(R.id.desayunoDosis);
        lunchDoss = findViewById(R.id.comidaDosis);
        dinnerDoss = findViewById(R.id.cenaDosis);
        bedDoss = findViewById(R.id.dormirDosis);

        initDate = findViewById(R.id.initDate);
        endDate = findViewById(R.id.endDate);

        initPiker = findViewById(R.id.diaInicio);
        endPicker = findViewById(R.id.diaFin);

        dosageSpinner = findViewById(R.id.drugDosageSpinner);

        btnAccept = findViewById(R.id.accept_btn);


        drugName.setText(drug.getDrugName());

        if (!drug.getAt_breakfast().equals("null") && !drug.getAt_breakfast().equals("0")){
            breakfastDoss.setText(drug.getAt_breakfast());
            breakfast.setChecked(true);
        }else{
            breakfastDoss.setText("0");
        }
        if (!drug.getAt_lunch().equals("null")&& !drug.getAt_lunch().equals("0")){
            lunchDoss.setText(drug.getAt_lunch());
            lunch.setChecked(true);
        }else{
            lunchDoss.setText("0");
        }

        if (!drug.getAt_dinner().equals("null") && !drug.getAt_dinner().equals("0") ){
            dinnerDoss.setText(drug.getAt_dinner());
            dinner.setChecked(true);
        }else{
            dinnerDoss.setText("0");
        }

        if (!drug.getAt_bedtime().equals("null") && !drug.getAt_bedtime().equals("0")){
            bedDoss.setText(drug.getAt_bedtime());
            bed.setChecked(true);
        }else{
            bedDoss.setText("0");
        }

        if (endDate.getText().equals("null") ||endDate.getText().equals("") ){
            endDate.setText("crónico");
        }else{
            endDate.setText(drug.getEnd_date());
        }




        initDate.setText(drug.getStart_date());



        initPiker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar calendar = Calendar.getInstance();
                final int day = calendar.get(Calendar.DAY_OF_MONTH);
                final int month=  calendar.get(Calendar.MONTH);
                final int year = calendar.get(Calendar.YEAR);

                DatePickerDialog datePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int y, int m, int d) {
                        initDate.setText(d + "/" + (m+1) + "/" + y);
                    }
                },year,month,day);

                datePickerDialog.show();
            }
        });

        endPicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar calendar = Calendar.getInstance();
                final int day = calendar.get(Calendar.DAY_OF_MONTH);
                final int month=  calendar.get(Calendar.MONTH);
                final int year = calendar.get(Calendar.YEAR);

                DatePickerDialog datePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int y, int m, int d) {
                        endDate.setText(d + "/" + (m+1) + "/" + y);
                    }
                },year,month,day);

                datePickerDialog.show();

            }
        });


        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNewPosologies();
            }
        });

    }




    private void addNewPosologies(){

        JSONObject param = new JSONObject();
        try {


            //param.put("drug_name",drug.getString("name"));

            if (breakfast.isChecked()){
                if(!breakfastDoss.getText().equals("")){
                    param.put("at_breakfast",breakfastDoss.getText().toString());
                }else{
                    Toast.makeText(context, "Introduzca unidades en el Desayuno",Toast.LENGTH_LONG).show();
                }
            }else{
                param.put("at_breakfast",0);
            }

            if (lunch.isChecked()){
                if(!lunchDoss.getText().equals("")){
                    param.put("at_lunch",lunchDoss.getText().toString());
                }else{
                    Toast.makeText(context, "Introduzca unidades en el Comida",Toast.LENGTH_LONG).show();
                }
            }else{
                param.put("at_lunch",0);
            }

            if (dinner.isChecked()){
                if(!dinnerDoss.getText().equals("")){
                    param.put("at_dinner",dinnerDoss.getText().toString());
                }else{
                    Toast.makeText(context, "Introduzca unidades en el Cena",Toast.LENGTH_LONG).show();
                }
            }else{
                param.put("at_dinner",0);
            }

            if (bed.isChecked()){
                if(!bedDoss.getText().equals("")){
                    param.put("at_bedtime",bedDoss.getText().toString());
                }else{
                    Toast.makeText(context, "Introduzca unidades en la Cena",Toast.LENGTH_LONG).show();
                }
            }else{
                param.put("at_bedtime",0);
            }
            //param.put("start_time",1);
            if(!initDate.getText().equals("")){

                String oldDateString = initDate.getText().toString();
                String newDateString;

                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                try{
                    Date d = sdf.parse(oldDateString);
                    sdf.applyPattern("yyyy-MM-dd");
                    newDateString = sdf.format(d);
                    param.put("start_date",newDateString);
                }catch (Exception e){
                    Log.d("PARSE", "Error en parsear fecha");
                }



            }else {
                Toast.makeText(context, "Introduzca una fecha de inicio",Toast.LENGTH_LONG).show();
            }

            if(!endDate.getText().equals("")){

                String oldDateString = endDate.getText().toString();
                String newDateString;

                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                try{
                    Date d = sdf.parse(oldDateString);
                    sdf.applyPattern("yyyy-MM-dd");
                    newDateString = sdf.format(d);
                    param.put("end_date",newDateString);
                }catch (Exception e){
                    Log.d("PARSE", "Error en parsear fecha");
                }


            }else {
                Toast.makeText(context, "Introduzca una fecha de inicio",Toast.LENGTH_LONG).show();
            }




            Log.d("FER", param.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        //int id = Integer.getInteger();
        URL += drug.getId()+"/";
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.PATCH, URL, param,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //   Toast.makeText(context, "Mensaje enviado",Toast.LENGTH_LONG).show();
                        //   Log.d("TAG","Success");

                        Toast.makeText(context, "Posología actualizada corectamente",Toast.LENGTH_LONG).show();
                        Intent intent= new Intent(context, DrugsMainView.class);
                        startActivity(intent);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                        if(error instanceof ServerError){
                            Log.d("TAG","Error en servidor");
                        }
                        if(error instanceof NoConnectionError){//no conexion internet
                            Log.d("TAG","No hay conexion a internet");
                        }

                        if(error instanceof NetworkError){//desconexionsocket
                            Log.d("TAG","NetWorkError");
                        }

                    }
                }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                //String credentials = "Dinna:12345678";
                //String auth = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                //headers.put("Authorization", auth);
                headers.put("Authorization", "Bearer" + " " + token);//App.getToken()
                return headers;
            }
        };

        VolleySingleton.getInstaceVolley(getApplicationContext()).addToRequestQueue(request);
    }

}